﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

using System.Net.Sockets;
using System.Text.Json;
using Json.Net;
using System.Threading;
using System.ComponentModel;


namespace client_WPF_
{
    /// <summary>
    /// Interaction logic for JoinRoomWindow.xaml
    /// </summary>
    public partial class JoinRoomWindow : Window
    {

        private Socket _clientSocket = null;

        private BackgroundWorker background_worker_rooms = new BackgroundWorker();
        private BackgroundWorker background_worker_players = new BackgroundWorker();

        private List<string> rooms = null;
        private List<string> players = null;

        string room_name = "";
        string userName;

        public JoinRoomWindow(Socket clientSocket, string username)
        {
            InitializeComponent();
            _clientSocket = clientSocket;
            userName = username;
            this.Title.Text = "Hello " + username;

            //rooms thread
            background_worker_rooms.WorkerSupportsCancellation = true;
            background_worker_rooms.WorkerReportsProgress = true;
            background_worker_rooms.DoWork += get_rooms_thread;
            background_worker_rooms.ProgressChanged += ProgressChanged_rooms;
            background_worker_rooms.RunWorkerCompleted += background_worker_RunWorkerCompleted;
            background_worker_rooms.RunWorkerAsync(); //activate rooms thread.
            
            //players thread
            background_worker_players.WorkerSupportsCancellation = true;
            background_worker_players.WorkerReportsProgress = true;
            background_worker_players.DoWork += get_players_thread;
            background_worker_players.ProgressChanged += ProgressChanged_players;
            background_worker_players.RunWorkerCompleted += background_worker_RunWorkerCompleted;

        }

        protected override void OnMouseLeftButtonDown(MouseButtonEventArgs e)
        {
            base.OnMouseLeftButtonDown(e);
            DragMove();
        }

        private void SendInfrmaionToServer(string userInfo)
        {
            if (_clientSocket.Connected)
            {
                // 1 convert the form informatino to byte array
                byte[] userData = Encoding.ASCII.GetBytes(userInfo);
                // send data to the server as byte array
                _clientSocket.Send(userData);
            }
        }

        private string ReciveInformationFromServer()
        {
            try
            {
                byte[] reciveBuffer = new byte[1024];
                int rec = _clientSocket.Receive(reciveBuffer);
                byte[] data = new byte[rec];
                Array.Copy(reciveBuffer, data, rec);
                string returnFormServer = Encoding.ASCII.GetString(data);
                return returnFormServer;
            }
            catch (Exception e)
            {
                //LBserverStatus.Text = "not ok";
            }
            return "";
        }

        private void Menu_Click(object sender, RoutedEventArgs e)
        {
            background_worker_rooms.CancelAsync();
            background_worker_players.CancelAsync();

            MenuWindow wnd = new MenuWindow(_clientSocket, userName);
            wnd.Top = this.Top;
            wnd.Left = this.Left;
            wnd.Show();
            this.Hide();
        }

        void get_rooms_thread(object sender, DoWorkEventArgs e)
        {
            while (true)
            {
                
                if (background_worker_rooms.CancellationPending)
                {
                    e.Cancel = true;
                    break;
                }
                
                string GetRoomsMessage = @"E0017{""Itamar"": ""Ori""}";
                SendInfrmaionToServer(GetRoomsMessage);
                string answer = ReciveInformationFromServer();

                if (answer[0] != 'E') continue; 
                
                string json = answer.Substring(5, answer.Length - 5);

                RoomList roomsNames = System.Text.Json.JsonSerializer.Deserialize<RoomList>(json);
                
                rooms = new List<string>();
                for (int i = 0; i < roomsNames.rooms.Length; i++) rooms.Add(roomsNames.rooms[i]);

                
                if (roomsNames.status == 1)
                {
                    background_worker_rooms.ReportProgress(1);
                }
                else
                {
                    background_worker_rooms.ReportProgress(0);
                }

                //MessageBox.Show("rooms list update successfully");

                Thread.Sleep(3000);
            }
        }

        void get_players_thread(object sender, DoWorkEventArgs e)
        {
            string json, length, message, answer;
            JoinRoom request;

            //send the message to be update.
            while (true)
            {

                if (background_worker_players.CancellationPending)
                {
                    e.Cancel = true;
                    break;
                }

                //build the message
                request = new JoinRoom(room_name);
                json = System.Text.Json.JsonSerializer.Serialize(request);
                length = json.Length.ToString();
                while (length.Length < 4)
                {
                    length = "0" + length;
                }

                message = "F" + length + json;
                SendInfrmaionToServer(message);
                answer = ReciveInformationFromServer();

                if (answer[0] != 'F') continue;
                
                answer = answer.Substring(5, answer.Length - 5);

                PlayersList roomsNames = System.Text.Json.JsonSerializer.Deserialize<PlayersList>(answer);

                players = new List<string>();
                for (int i = 0; i < roomsNames.players.Length; i++) players.Add(roomsNames.players[i]);


                background_worker_players.ReportProgress(1);

                //MessageBox.Show("players list update successfully");

                Thread.Sleep(3000);
            }
        }

        void ProgressChanged_rooms(object sender, ProgressChangedEventArgs e)
        {
            if (e.ProgressPercentage == 1)
            {
                this.RoomsLb.ItemsSource = rooms;
                txtErrorMsg.Text = "";
            }
            else txtErrorMsg.Text = "There are no available rooms.";
        }

        void ProgressChanged_players(object sender, ProgressChangedEventArgs e)
        {
            this.PlayersLB.ItemsSource = players;
        }

        private void LB_Playlist_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {//SelectionChangedEventArgs
            if (room_name == "") background_worker_players.RunWorkerAsync(); //activate players thread.
            room_name = RoomsLb.SelectedItem.ToString();
        }

        void DataWindow_Closing(object sender, CancelEventArgs e)
        {
            Environment.Exit(0);
        }

        void background_worker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (e.Cancelled)
            {
                //MessageBox.Show("BackgroundWorker canceled");
            }
            else
            {
                //MessageBox.Show("BackgroundWorker ended successfully");
            }
        }

        private void JoinBtn_Click(object sender, RoutedEventArgs e)
        {
            string json, length, message, answer;
            JoinRoom request;

            if (RoomsLb.Items.Count == 0 || RoomsLb.SelectedIndex == -1) return;

            request = new JoinRoom(RoomsLb.SelectedItem.ToString());
            json = System.Text.Json.JsonSerializer.Serialize(request);
            length = json.Length.ToString();

            while (length.Length < 4)
            {
                length = "0" + length;
            }

            message = "G" + length + json;
            SendInfrmaionToServer(message);
            answer = ReciveInformationFromServer();

            if (answer != @"G0016{""status"":1}") txtErrorMsg.Text = "this Room is full";
            else
            {
                background_worker_rooms.CancelAsync();
                background_worker_players.CancelAsync();

                MemberWaitingRoom wnd = new MemberWaitingRoom(_clientSocket, RoomsLb.SelectedItem.ToString(), userName);
                wnd.Top = this.Top;
                wnd.Left = this.Left;
                wnd.Show();
                this.Hide();
            }
        }

        private void CreateRoomBtn_Checked(object sender, RoutedEventArgs e)
        {
            background_worker_rooms.CancelAsync();
            background_worker_players.CancelAsync();

            CreateRoomWindow wnd = new CreateRoomWindow(_clientSocket, userName);
            wnd.Top = this.Top;
            wnd.Left = this.Left;
            wnd.Show();
            this.Hide();
        }

        private void LogOutBtns_Checked(object sender, RoutedEventArgs e)
        {
            background_worker_rooms.CancelAsync();
            background_worker_players.CancelAsync();

            string LogoutMessage = @"D0017{""Itamar"": ""Ori""}";
            SendInfrmaionToServer(LogoutMessage);
            string answer = ReciveInformationFromServer();
            
            LoginWindow wnd = new LoginWindow(_clientSocket);
            wnd.Top = this.Top;
            wnd.Left = this.Left;
            wnd.Show();
            this.Hide();
        }

        private void StatisticsBtn_Checked(object sender, RoutedEventArgs e)
        {
            background_worker_rooms.CancelAsync();
            background_worker_players.CancelAsync();

            StatisticsWindow wnd = new StatisticsWindow(_clientSocket, userName);
            wnd.Top = this.Top;
            wnd.Left = this.Left;
            wnd.Show();
            this.Hide();
        }

        private void HomeBtn_Checked(object sender, RoutedEventArgs e)
        {
            background_worker_rooms.CancelAsync();
            background_worker_players.CancelAsync();

            MenuWindow wnd = new MenuWindow(_clientSocket, userName);
            wnd.Top = this.Top;
            wnd.Left = this.Left;
            wnd.Show();
            this.Hide();
        }

        private void CreditsBtn_Checked(object sender, RoutedEventArgs e)
        {
            background_worker_rooms.CancelAsync();
            background_worker_players.CancelAsync();

            CreditsWindow wnd = new CreditsWindow(_clientSocket, userName);
            wnd.Top = this.Top;
            wnd.Left = this.Left;
            wnd.Show();
            this.Hide();
        }

        private void LeaderboardBtn_Checked(object sender, RoutedEventArgs e)
        {
            background_worker_rooms.CancelAsync();
            background_worker_players.CancelAsync();

            LeaderboardWindow wnd = new LeaderboardWindow(_clientSocket, userName);
            wnd.Top = this.Top;
            wnd.Left = this.Left;
            wnd.Show();
            this.Hide();
        }

        private void PlayersLB_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (room_name == "") background_worker_players.RunWorkerAsync(); //activate players thread.
            room_name = RoomsLb.SelectedItem.ToString();
        }
    }
}