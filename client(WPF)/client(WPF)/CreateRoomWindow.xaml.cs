﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;


using System.Text.Json;
using System.Net.Sockets;

namespace client_WPF_
{
    /// <summary>
    /// Interaction logic for CreateRoomWindow.xaml
    /// </summary>
    public partial class CreateRoomWindow : Window
    {
        private Socket _clientSocket = null;
        string userName;

        public CreateRoomWindow(Socket clientSocket, string username)
        {
            InitializeComponent();
            _clientSocket = clientSocket;
            userName = username;
            this.Title.Text = "Hello " + username;
        }

        protected override void OnMouseLeftButtonDown(MouseButtonEventArgs e)
        {
            base.OnMouseLeftButtonDown(e);
            DragMove();
        }

        private void SendInfrmaionToServer(string userInfo)
        {
            if (_clientSocket.Connected)
            {
                // 1 convert the form informatino to byte array
                byte[] userData = Encoding.ASCII.GetBytes(userInfo);
                // send data to the server as byte array
                _clientSocket.Send(userData);
            }
        }

        private string ReciveInformationFromServer()
        {
            try
            {
                byte[] reciveBuffer = new byte[1024];
                int rec = _clientSocket.Receive(reciveBuffer);
                byte[] data = new byte[rec];
                Array.Copy(reciveBuffer, data, rec);
                string returnFormServer = Encoding.ASCII.GetString(data);
                return returnFormServer;
            }
            catch (Exception e)
            {
                //LBserverStatus.Text = "not ok";
            }
            return "";
        }

        private void Menu_Click(object sender, RoutedEventArgs e)
        {
            MenuWindow wnd = new MenuWindow(_clientSocket, userName);
            wnd.Top = this.Top;
            wnd.Left = this.Left;
            wnd.Show();
            this.Hide();
        }

        private void displayProblem()
        {
            if((int)questionsS.Value == 0) this.txtErrorMsg.Text = "The amount of questions can not be zero.";
            if((int)timeS.Value == 0) this.txtErrorMsg.Text = "The amount of minutes per question can not be zero.";
            if((int)usersS.Value == 0) this.txtErrorMsg.Text = "The Max user amount can't be zero.";
            if(txtRoomsName.Text.Length == 0) this.txtErrorMsg.Text = "The room field is empty.";
        }

        private void OnKeyDownHandler(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Return)
            {
                createBtn_Click(sender, e);
            }
        }

        private void createBtn_Click(object sender, RoutedEventArgs e)
        {
            if (txtRoomsName.Text.Length == 0 || (int)usersS.Value == 0 || (int)timeS.Value == 0 || (int)questionsS.Value == 0)
            {
                displayProblem();
                return;
            }

            CreateRoom request = new CreateRoom(txtRoomsName.Text, (int)usersS.Value, (int)timeS.Value, (int)questionsS.Value);
            string json = JsonSerializer.Serialize(request);
            string length = json.Length.ToString();
            string message;

            if (json.Length > 9999)
            {
                txtErrorMsg.Text = "This room name is too long.";
                return;
            }
            while (length.Length < 4)
            {
                length = "0" + length;
            }

            message = "H" + length + json;
            SendInfrmaionToServer(message);

            string answer = ReciveInformationFromServer();

            if (!(answer.Length != 0 && answer[answer.Length - 2] == '1')) txtErrorMsg.Text = "Your room name is taken, try again.";
            else
            {
                AdminWaitingRoom wnd = new AdminWaitingRoom(_clientSocket, txtRoomsName.Text, userName);
                wnd.Top = this.Top;
                wnd.Left = this.Left;
                wnd.Show();
                this.Hide();
            }
        }

        private void JoinRoomBtn_Checked(object sender, RoutedEventArgs e)
        {
            JoinRoomWindow wnd = new JoinRoomWindow(_clientSocket, userName);
            wnd.Top = this.Top;
            wnd.Left = this.Left;
            wnd.Show();
            this.Hide();
        }

        private void Leaderboard_Checked(object sender, RoutedEventArgs e)
        {
            LeaderboardWindow wnd = new LeaderboardWindow(_clientSocket, userName);
            wnd.Top = this.Top;
            wnd.Left = this.Left;
            wnd.Show();
            this.Hide();
        }

        private void LogOutBtns_Checked(object sender, RoutedEventArgs e)
        {
            string LogoutMessage = @"D0017{""Itamar"": ""Ori""}";
            SendInfrmaionToServer(LogoutMessage);
            string answer = ReciveInformationFromServer();

            LoginWindow wnd = new LoginWindow(_clientSocket);
            wnd.Top = this.Top;
            wnd.Left = this.Left;
            wnd.Show();
            this.Hide();
        }

        private void StatisticsBtn_Checked(object sender, RoutedEventArgs e)
        {
            StatisticsWindow wnd = new StatisticsWindow(_clientSocket, userName);
            wnd.Top = this.Top;
            wnd.Left = this.Left;
            wnd.Show();
            this.Hide();
        }

        private void HomeBtn_Checked(object sender, RoutedEventArgs e)
        {
            MenuWindow wnd = new MenuWindow(_clientSocket, userName);
            wnd.Top = this.Top;
            wnd.Left = this.Left;
            wnd.Show();
            this.Hide();
        }

        private void CreditsBtn_Checked(object sender, RoutedEventArgs e)
        {
            CreditsWindow wnd = new CreditsWindow(_clientSocket, userName);
            wnd.Top = this.Top;
            wnd.Left = this.Left;
            wnd.Show();
            this.Hide();
        }
    }
}