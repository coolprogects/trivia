﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

using System.Net.Sockets;
using System.Text.Json;
using Json.Net;
using System.Threading;
using System.ComponentModel;

namespace client_WPF_
{
    /// <summary>
    /// Interaction logic for MemberWaitingRoom.xaml
    /// </summary>
    public partial class MemberWaitingRoom : Window
    {

        private Socket _clientSocket = null;

        private BackgroundWorker background_worker_players = new BackgroundWorker();
       
        private List<string> players = null;
        int isActive = 0;
        string roomName;
        string userName;
        int time;

        RoomState RoomInfo;

        public MemberWaitingRoom(Socket clientSocket, string room_name, string username)
        {
            InitializeComponent();


            _clientSocket = clientSocket;
            roomName = room_name;
            this.txtRoomsName.Text += room_name;
            userName = username;
            this.Title.Text = "Hello: " + username;

            //players thread
            background_worker_players.WorkerSupportsCancellation = true;
            background_worker_players.WorkerReportsProgress = true;
            background_worker_players.DoWork += get_players_thread;
            background_worker_players.ProgressChanged += ProgressChanged_players;
            background_worker_players.RunWorkerCompleted += background_worker_RunWorkerCompleted;
            background_worker_players.RunWorkerAsync();

        }

        protected override void OnMouseLeftButtonDown(MouseButtonEventArgs e)
        {
            base.OnMouseLeftButtonDown(e);
            DragMove();
        }

        private void SendInfrmaionToServer(string userInfo)
        {
            if (_clientSocket.Connected)
            {
                byte[] userData = Encoding.ASCII.GetBytes(userInfo);
                _clientSocket.Send(userData);
            }
        }

        private string ReciveInformationFromServer()
        {
            try
            {
                byte[] reciveBuffer = new byte[1024];
                int rec = _clientSocket.Receive(reciveBuffer);
                byte[] data = new byte[rec];
                Array.Copy(reciveBuffer, data, rec);
                string returnFormServer = Encoding.ASCII.GetString(data);
                return returnFormServer;
            }
            catch (Exception e)
            {
                //LBserverStatus.Text = "not ok";
            }
            return "";
        }


        void get_players_thread(object sender, DoWorkEventArgs e)
        {
            string json, length, message, answer;
            JoinRoom request;

            //send the message to be update.
            while (true)
            {

                if (background_worker_players.CancellationPending)
                {
                    e.Cancel = true;
                    break;
                }

                message = @"L0017{""Itamar"": ""Ori""}";
                SendInfrmaionToServer(message);
                answer = ReciveInformationFromServer();

                if (answer[0] != 'L') continue;

                json = answer.Substring(5, answer.Length - 5);

                RoomInfo = System.Text.Json.JsonSerializer.Deserialize<RoomState>(json);

                time = RoomInfo.answerTimeout;
                isActive = RoomInfo.hasGameBegun;
                players = new List<string>();
                for (int i = 0; i < RoomInfo.players.Length; i++) players.Add(RoomInfo.players[i]);
                background_worker_players.ReportProgress(1);
                
                Thread.Sleep(3000);
            }
        }

      
        void ProgressChanged_players(object sender, ProgressChangedEventArgs e)
        {
            this.PlayersListLB.ItemsSource = players;
            if (isActive == 2)
            {
                background_worker_players.CancelAsync();

                string message = @"N0017{""Itamar"": ""Ori""}";
                SendInfrmaionToServer(message);
                ReciveInformationFromServer();

                MenuWindow wnd = new MenuWindow(_clientSocket, userName);
                wnd.Top = this.Top;
                wnd.Left = this.Left;
                wnd.Show();
                this.Hide();
            };

            if (isActive == 1)
            {
                background_worker_players.CancelAsync();
                MemberGameWindow wnd = new MemberGameWindow(_clientSocket, roomName, userName, RoomInfo);
                wnd.Top = this.Top;
                wnd.Left = this.Left;
                wnd.Show();
                this.Hide();
            }
        }

        void background_worker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (e.Cancelled)
            {
                //MessageBox.Show("BackgroundWorker_players canceled");
            }
            else
            {
                //MessageBox.Show("BackgroundWorker ended successfully");
            }
        }

        void DataWindow_Closing(object sender, CancelEventArgs e)
        {
            Environment.Exit(0);
        }

        private void LeaveRoomBtn_Click(object sender, RoutedEventArgs e)
        {
            string json, length, message, answer;

            background_worker_players.CancelAsync();

            json = @"N0017{""Itamar"": ""Ori""}";
            SendInfrmaionToServer(json);
            answer = ReciveInformationFromServer();

            MenuWindow wnd = new MenuWindow(_clientSocket, userName);
            wnd.Top = this.Top;
            wnd.Left = this.Left;
            wnd.Show();
            this.Hide();
        }

    }
}