﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using MaterialDesignThemes.Wpf;
using System.Text.Json;
using System.Net.Sockets;
using System.Net;

namespace client_WPF_
{
    /// <summary>
    /// Interaction logic for LoginWindow.xaml
    /// </summary>
    public partial class LoginWindow : Window
    {

        private Socket _clientSocket = null;
        enum codes {ERROR = 'A', LOGIN = 'B', SIGNUP = 'C' }
        public bool IsDarkTheme { get; set; }
        private readonly PaletteHelper paletteHelper = new PaletteHelper();

        public LoginWindow()
        {
            InitializeComponent();

            _clientSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            ConnectToServer();
        }

        public LoginWindow(Socket clientSocket)
        {
            _clientSocket = clientSocket;
            InitializeComponent();
        }

        private void ConnectToServer()
        {
            if (!_clientSocket.Connected)
            {
                try
                {
                    _clientSocket.Connect(IPAddress.Loopback, 55555);
                }
                catch (SocketException)
                {
                    //l2.Text = "conection state: not ok";
                }
                if (_clientSocket.Connected)
                {
                    //l2.Text = "conection state: ok";
                }
            }
        }

        private void SendInfrmaionToServer(string userInfo)
        {
            if (_clientSocket.Connected)
            {
                // 1 convert the form informatino to byte array
                byte[] userData = Encoding.ASCII.GetBytes(userInfo);
                // send data to the server as byte array
                _clientSocket.Send(userData);
            }
        }

        private string ReciveInformationFromServer()
        {
            try
            {
                //_clientSocket.Connect(IPAddress.Loopback, 50000);
                //preper to recive data from the server
                //1.preper byte array to get all the bytes from the servr
                byte[] reciveBuffer = new byte[1024];
                //2.recive the data from the server in to the byte array and
                //return the size of bvtes how recive
                int rec = _clientSocket.Receive(reciveBuffer);
                //3. preper byte array with the size of bytes how recive frm
                //the servr
                byte[] data = new byte[rec];
                //4. copy the byte array how reive in to the byte array with
                //the correct size
                Array.Copy(reciveBuffer, data, rec);
                //5. convert the byte array to Ascii
                string returnFormServer = Encoding.ASCII.GetString(data);
                return returnFormServer;
            }
            catch (Exception e)
            {
                //LBserverStatus.Text = "not ok";
            }
            return "";
        }

        private void exitApp(object sender, RoutedEventArgs e)
        {
            Application.Current.Shutdown();
        }

        protected override void OnMouseLeftButtonDown(MouseButtonEventArgs e)
        {
            base.OnMouseLeftButtonDown(e);
            DragMove();
        }

        private void OnKeyDownHandler(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Return)
            {
                loginBtn_Click(sender, e);
            }
        }

        private void loginBtn_Click(object sender, RoutedEventArgs e)
        {
            loginUser newUser = new loginUser(this.txtUsername.Text, this.txtPassword.Password);
            string json = JsonSerializer.Serialize(newUser);
            string length = json.Length.ToString();
            string message;

            if (json.Length > 9999)
            {
                this.txtErrorMsg.Text = "The username and password are too long.";
                return;
            }
            while (length.Length < 4)
            {
                length = "0" + length;
            }

            message = (char)codes.LOGIN + length + json;
            SendInfrmaionToServer(message);

            string answer = ReciveInformationFromServer();
            if (!(answer.Length != 0 && answer[0] == 'B')) this.txtErrorMsg.Text = "Your username or\npassword was incorrect,\nplease try again.";
            else
            {
                MenuWindow wnd = new MenuWindow(_clientSocket, this.txtUsername.Text);
                wnd.Top = this.Top;
                wnd.Left = this.Left;
                wnd.Show();
                this.Hide();
            }
        }

        private void signupBtn_Click(object sender, RoutedEventArgs e)
        {
            SignupWindow wnd = new SignupWindow(_clientSocket);
            wnd.Top = this.Top;
            wnd.Left = this.Left;
            wnd.Show();
            this.Hide();
        }
    }
}
