﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Net.Sockets;
using System.Text.Json;
using System.Windows.Navigation;

namespace client_WPF_
{
    /// <summary>
    /// Interaction logic for MenuWindow.xaml
    /// </summary>
    public partial class MenuWindow : Window
    {
        private Socket _clientSocket = null;
        string userName;

        public MenuWindow(Socket clientSocket, string username)
        {
            InitializeComponent();
            _clientSocket = clientSocket;
            userName = username;
            this.Title.Text = "Hello " + username;
        }


        private void SendInfrmaionToServer(string userInfo)
        {
            if (_clientSocket.Connected)
            {
                // 1 convert the form informatino to byte array
                byte[] userData = Encoding.ASCII.GetBytes(userInfo);
                // send data to the server as byte array
                _clientSocket.Send(userData);
            }
        }

        private string ReciveInformationFromServer()
        {
            try
            {
                //_clientSocket.Connect(IPAddress.Loopback, 50000);
                //preper to recive data from the server
                //1.preper byte array to get all the bytes from the servr
                byte[] reciveBuffer = new byte[1024];
                //2.recive the data from the server in to the byte array and
                //return the size of bvtes how recive
                int rec = _clientSocket.Receive(reciveBuffer);
                //3. preper byte array with the size of bytes how recive frm
                //the servr
                byte[] data = new byte[rec];
                //4. copy the byte array how reive in to the byte array with
                //the correct size
                Array.Copy(reciveBuffer, data, rec);
                //5. convert the byte array to Ascii
                string returnFormServer = Encoding.ASCII.GetString(data);
                return returnFormServer;
            }
            catch (Exception e)
            {
                //LBserverStatus.Text = "not ok";
            }
            return "";
        }

        protected override void OnMouseLeftButtonDown(MouseButtonEventArgs e)
        {
            base.OnMouseLeftButtonDown(e);
            DragMove();
        }

        private void LogOutBtns_Checked(object sender, RoutedEventArgs e)
        {
            string LogoutMessage = @"D0017{""Itamar"": ""Ori""}";
            SendInfrmaionToServer(LogoutMessage);
            string answer = ReciveInformationFromServer();

            LoginWindow wnd = new LoginWindow(_clientSocket);
            wnd.Top = this.Top;
            wnd.Left = this.Left;
            wnd.Show();
            this.Hide();
        }

        private void HomeBtn_Checked(object sender, RoutedEventArgs e)
        {
            MenuWindow wnd = new MenuWindow(_clientSocket, userName);
            wnd.Top = this.Top;
            wnd.Left = this.Left;
            wnd.Show();
            this.Hide();
        }

        private void StatisticsBtn_Checked(object sender, RoutedEventArgs e)
        {
            StatisticsWindow wnd = new StatisticsWindow(_clientSocket, userName);
            wnd.Top = this.Top;
            wnd.Left = this.Left;
            wnd.Show();
            this.Hide();
        }

        private void CreditsBtn_Checked(object sender, RoutedEventArgs e)
        {
            CreditsWindow wnd = new CreditsWindow(_clientSocket, userName);
            wnd.Top = this.Top;
            wnd.Left = this.Left;
            wnd.Show();
            this.Hide();
        }

        private void LeaderboardBtn_Checked(object sender, RoutedEventArgs e)
        {
            LeaderboardWindow wnd = new LeaderboardWindow(_clientSocket, userName);
            wnd.Top = this.Top;
            wnd.Left = this.Left;
            wnd.Show();
            this.Hide();
        }

        private void CreateRoomBtn_Checked(object sender, RoutedEventArgs e)
        {
            CreateRoomWindow wnd = new CreateRoomWindow(_clientSocket, userName);
            wnd.Top = this.Top;
            wnd.Left = this.Left;
            wnd.Show();
            this.Hide();
        }

        private void JoinRoomBtn_Checked(object sender, RoutedEventArgs e)
        {
            JoinRoomWindow wnd = new JoinRoomWindow(_clientSocket, userName);
            wnd.Top = this.Top;
            wnd.Left = this.Left;
            wnd.Show();
            this.Hide();
        }
    }
}
