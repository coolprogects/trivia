﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Net.Sockets;

namespace client_WPF_
{
    /// <summary>
    /// Interaction logic for CreditsWindow.xaml
    /// </summary>
    public partial class CreditsWindow : Window
    {
        private Socket _clientSocket = null;
        string userName;

        public CreditsWindow(Socket clientSocket, string username)
        {
            InitializeComponent();
            _clientSocket = clientSocket;
            userName = username;
            this.Title.Text = "Hello " + userName;
        }

        protected override void OnMouseLeftButtonDown(MouseButtonEventArgs e)
        {
            base.OnMouseLeftButtonDown(e);
            DragMove();
        }

        private void SendInfrmaionToServer(string userInfo)
        {
            if (_clientSocket.Connected)
            {
                byte[] userData = Encoding.ASCII.GetBytes(userInfo);
                _clientSocket.Send(userData);
            }
        }

        private string ReciveInformationFromServer()
        {
            try
            {
                byte[] reciveBuffer = new byte[1024];
                int rec = _clientSocket.Receive(reciveBuffer);
                byte[] data = new byte[rec];
                Array.Copy(reciveBuffer, data, rec);
                string returnFormServer = Encoding.ASCII.GetString(data);
                return returnFormServer;
            }
            catch (Exception e)
            {
                //LBserverStatus.Text = "not ok";
            }
            return "";
        }

        private void JoinRoomBtn_Checked(object sender, RoutedEventArgs e)
        {
            JoinRoomWindow wnd = new JoinRoomWindow(_clientSocket, userName);
            wnd.Top = this.Top;
            wnd.Left = this.Left;
            wnd.Show();
            this.Hide();
        }
        private void CreateRoomBtn_Checked(object sender, RoutedEventArgs e)
        {
            CreateRoomWindow wnd = new CreateRoomWindow(_clientSocket, userName);
            wnd.Top = this.Top;
            wnd.Left = this.Left;
            wnd.Show();
            this.Hide();
        }

        private void LeaderboardBtn_Checked(object sender, RoutedEventArgs e)
        {
            LeaderboardWindow wnd = new LeaderboardWindow(_clientSocket, userName);
            wnd.Top = this.Top;
            wnd.Left = this.Left;
            wnd.Show();
            this.Hide();
        }

        private void LogOutBtns_Checked(object sender, RoutedEventArgs e)
        {
            string LogoutMessage = @"D0017{""Itamar"": ""Ori""}";
            SendInfrmaionToServer(LogoutMessage);
            string answer = ReciveInformationFromServer();

            LoginWindow wnd = new LoginWindow(_clientSocket);
            wnd.Top = this.Top;
            wnd.Left = this.Left;
            wnd.Show();
            this.Hide();
        }

        private void StatisticsBtn_Checked(object sender, RoutedEventArgs e)
        {
            StatisticsWindow wnd = new StatisticsWindow(_clientSocket, userName);
            wnd.Top = this.Top;
            wnd.Left = this.Left;
            wnd.Show();
        }

        private void HomeBtn_Checked(object sender, RoutedEventArgs e)
        {
            MenuWindow wnd = new MenuWindow(_clientSocket, userName);
            wnd.Top = this.Top;
            wnd.Left = this.Left;
            wnd.Show();
            this.Hide();
        }

        private void CreditsBtn_Checked(object sender, RoutedEventArgs e)
        {
            CreditsWindow wnd = new CreditsWindow(_clientSocket, userName);
            wnd.Top = this.Top;
            wnd.Left = this.Left;
            wnd.Show();
            this.Hide();
        }

    }
}
