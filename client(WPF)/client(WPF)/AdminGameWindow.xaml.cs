﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

using System.Net.Sockets;
using System.Text.Json;
using Json.Net;
using System.Threading;
using System.ComponentModel;
using Newtonsoft.Json;

namespace client_WPF_
{
    /// <summary>
    /// Interaction logic for AdminGameWindow.xaml
    /// </summary>
    public partial class AdminGameWindow : Window
    {

        private Socket _clientSocket = null;
        private BackgroundWorker background_worker_question = new BackgroundWorker();
        string roomName;
        string userName;
        int timeForQuestion;
        string[] answers = new string[4];
        string Question = "Wait a moment.";
        int count = 0;
        int timeLeft;
        int questionAmount;
        int playersAmount;
        int correctAnswers;

        public AdminGameWindow(Socket clientSocket, string room_name, string username, RoomState RoomInfo)
        {
            InitializeComponent();

            playersAmount = RoomInfo.players.Length;
            timeForQuestion = RoomInfo.answerTimeout;
            _clientSocket = clientSocket;
            roomName = room_name;
            userName = username;
            questionAmount = RoomInfo.questionCount;
            rName.Content += roomName;
            pName.Content += userName;
            correctAnswers = 0;

            //Question thread.
            background_worker_question.WorkerSupportsCancellation = true;
            background_worker_question.WorkerReportsProgress = true;

            background_worker_question.DoWork += get_question_thread;
            background_worker_question.ProgressChanged += ProgressChanged_question;
            background_worker_question.RunWorkerCompleted += background_worker_RunWorkerCompleted;
            background_worker_question.RunWorkerAsync();            

        }

        private void SendInfrmaionToServer(string userInfo)
        {
            if (_clientSocket.Connected)
            {
                byte[] userData = Encoding.ASCII.GetBytes(userInfo);
                _clientSocket.Send(userData);
            }
        }

        private string ReciveInformationFromServer()
        {
            try
            {
                byte[] reciveBuffer = new byte[1024];
                int rec = _clientSocket.Receive(reciveBuffer);
                byte[] data = new byte[rec];
                Array.Copy(reciveBuffer, data, rec);
                string returnFormServer = Encoding.ASCII.GetString(data);
                return returnFormServer;
            }
            catch (Exception e)
            {
                //LBserverStatus.Text = "not ok";
            }
            return "";
        }

        void get_question_thread(object sender, DoWorkEventArgs e)
        {
            string json, length, message, answer;
            JoinRoom request;

            //send the message to be update.
            while (true)
            {
                timeLeft = timeForQuestion;
                if (background_worker_question.CancellationPending)
                {
                    e.Cancel = true;
                    break;
                }

                message = @"Q0017{""Itamar"": ""Ori""}";
                SendInfrmaionToServer(message);
                answer = ReciveInformationFromServer();

                if (answer[0] != 'Q') continue;

                json = answer.Substring(5, answer.Length - 5);
                
                dynamic json2 = JsonConvert.DeserializeObject(json);

                var status = (int)json2.status;
                if (status == 0) break;
                
                answers[0] = (string)json2.answers[0][1];
                answers[1] = (string)json2.answers[1][1];
                answers[2] = (string)json2.answers[2][1];
                answers[3] = (string)json2.answers[3][1];

                Question = (string)json2.question;



                count++;
                background_worker_question.ReportProgress(1);

                while (0 != timeLeft)
                {
                    background_worker_question.ReportProgress(0);
                    Thread.Sleep(1000);
                    timeLeft--;
                }
            }
        }

        protected override void OnMouseLeftButtonDown(MouseButtonEventArgs e)
        {
            base.OnMouseLeftButtonDown(e);
            DragMove();
        }

        void ProgressChanged_question(object sender, ProgressChangedEventArgs e)
        {
            Random r = new Random();
            string exist = "";
            uint num;

            if (e.ProgressPercentage == 0)
            {
                this.Timer.Text = timeLeft.ToString() + "s";
                return;
            } 

            Answer1.Text = "";
            Answer2.Text = "";
            Answer3.Text = "";
            Answer4.Text = "";

            Answer1Btn.IsEnabled = true;
            Answer2Btn.IsEnabled = true;
            Answer3Btn.IsEnabled = true;
            Answer4Btn.IsEnabled = true;


            while (!(exist.Contains("0") && exist.Contains("1") && exist.Contains("2") && exist.Contains("3")))
            {
                num = (uint)r.Next(4);
                if (exist.Contains(num.ToString())) continue;
                if (Answer1.Text == "") Answer1.Text = answers[num];
                else if (Answer2.Text == "") Answer2.Text = answers[num];
                else if (Answer3.Text == "") Answer3.Text = answers[num];
                else if (Answer4.Text == "") Answer4.Text = answers[num];
                exist += num;
            }

            qAmount.Text = count.ToString() + "/" + questionAmount.ToString();
            this.question.Text = Question;
        }

        void background_worker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (e.Cancelled)
            {
                //MessageBox.Show("You Left the game successfuly.");
            }
            else
            {
                if (playersAmount > 1)
                {
                    GameResultWindow wnd = new GameResultWindow(_clientSocket, userName);
                    wnd.Top = this.Top;
                    wnd.Left = this.Left;
                    wnd.Show();
                    this.Hide();
                }
                else
                {
                    SingleGameResultWindow wnd = new SingleGameResultWindow(_clientSocket, userName);
                    wnd.Top = this.Top;
                    wnd.Left = this.Left;
                    wnd.Show();
                    this.Hide();
                }
            }
        }

        void DataWindow_Closing(object sender, CancelEventArgs e)
        {
            Environment.Exit(0);
        }

        private void ans1_check(object sender, RoutedEventArgs e)
        {
            submit(Answer1.Text);
        }
        private void ans2_check(object sender, RoutedEventArgs e)
        {
            submit(Answer2.Text);
        }
        private void ans3_check(object sender, RoutedEventArgs e)
        {
            submit(Answer3.Text);
        }
        private void ans4_check(object sender, RoutedEventArgs e)
        {
            submit(Answer4.Text);
        }

        private void submit(string choice)
        {
            int id;
            string answer, message, length;

            id = Array.IndexOf(answers, choice) + 1;
            submitRequest request = new submitRequest(id, timeForQuestion - timeLeft);

            message = System.Text.Json.JsonSerializer.Serialize(request);
            length = message.Length.ToString();

            while (length.Length < 4)
            {
                length = "0" + length;
            }

            message = "P" + length + message;
            SendInfrmaionToServer(message);
            answer = ReciveInformationFromServer();
            answer = answer.Substring(5, answer.Length - 5);

            submitResponse response = System.Text.Json.JsonSerializer.Deserialize<submitResponse>(answer);
            if (id == response.correctAnswerId) correctAnswers++;
            
            Answer1Btn.IsEnabled = false;
            Answer2Btn.IsEnabled = false;
            Answer3Btn.IsEnabled = false;
            Answer4Btn.IsEnabled = false;

            cAmount.Text = correctAnswers.ToString();
        }

        private void LeaveGameBtn_Checked(object sender, RoutedEventArgs e)
        {
            string message, answer;

            background_worker_question.CancelAsync();

            message = @"R0017{""Itamar"": ""Ori""}";
            SendInfrmaionToServer(message);
            answer = ReciveInformationFromServer();

            MenuWindow wnd = new MenuWindow(_clientSocket, userName);
            wnd.Top = this.Top;
            wnd.Left = this.Left;
            wnd.Show();
            this.Hide();
        }

    }
}
