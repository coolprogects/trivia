﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

using System.Net.Sockets;
using System.Text.Json;
using Json.Net;
using System.Threading;
using System.ComponentModel;
using Newtonsoft.Json;

namespace client_WPF_
{
    /// <summary>
    /// Interaction logic for GameResultWindow.xaml
    /// </summary>
    public partial class GameResultWindow : Window
    {

        private Socket _clientSocket = null;
        string userName;
        private List<string> players2 = null;

        public GameResultWindow(Socket clientSocket, string user)
        {
            InitializeComponent();

            _clientSocket = clientSocket;
            userName = user;
            
            getResult();
        }


        private void SendInfrmaionToServer(string userInfo)
        {
            if (_clientSocket.Connected)
            {
                byte[] userData = Encoding.ASCII.GetBytes(userInfo);
                _clientSocket.Send(userData);
            }
        }

        private string ReciveInformationFromServer()
        {
            try
            {
                byte[] reciveBuffer = new byte[1024];
                int rec = _clientSocket.Receive(reciveBuffer);
                byte[] data = new byte[rec];
                Array.Copy(reciveBuffer, data, rec);
                string returnFormServer = Encoding.ASCII.GetString(data);
                return returnFormServer;
            }
            catch (Exception e)
            {
                //LBserverStatus.Text = "not ok";
            }
            return "";
        }


        public void getResult()
        {
            string message, answer, json;

            message = @"O0017{""Itamar"": ""Ori""}";
            SendInfrmaionToServer(message);
            answer = ReciveInformationFromServer();


            answer = answer.Substring(16, answer.Length - 16);
            answer = answer.Substring(0, answer.Length - 12);


            var dict = JsonConvert.DeserializeObject<Dictionary<string, float[]>>(answer);
            
           
            
            string[] data = new string[dict.Count];
            string winner = userName;
            players2 = new List<string>();
            
            foreach (KeyValuePair<string, float[]> entry in dict)
            {
                if (entry.Value[0] > dict[winner][0])
                {
                    winner = entry.Key;
                }
                else if (entry.Value[0] == dict[winner][0] && entry.Value[2] < dict[winner][2])
                {
                    winner = entry.Key;
                }
                players2.Add("Name: " + entry.Key + "  |  correct: " + entry.Value[0] + "  |  timePerQ: " + entry.Value[2].ToString("0.00"));
            }

            this.txtWinner.Text= "The winner is " + winner;
            this.PlayersLB.ItemsSource = players2;
        }

        private void HomeBtn_Checked(object sender, RoutedEventArgs e)
        {
            MenuWindow wnd = new MenuWindow(_clientSocket, userName);
            wnd.Top = this.Top;
            wnd.Left = this.Left;
            wnd.Show();
            this.Hide();
        }

        protected override void OnMouseLeftButtonDown(MouseButtonEventArgs e)
        {
            base.OnMouseLeftButtonDown(e);
            DragMove();
        }
    }
}
