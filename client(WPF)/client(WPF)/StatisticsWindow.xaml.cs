﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

using System.Net.Sockets;
using System.Text.Json;

namespace client_WPF_
{
    /// <summary>
    /// Interaction logic for StatisticsWindow.xaml
    /// </summary>
    public partial class StatisticsWindow : Window
    {

        private Socket _clientSocket = null;
        string userName;

        public StatisticsWindow(Socket clientSocket, string username)
        {
            InitializeComponent();
            
            _clientSocket = clientSocket;
            userName = username;
            this.Title.Text = "Hello " + username;
            get_statistic();
        }

        private void SendInfrmaionToServer(string userInfo)
        {
            if (_clientSocket.Connected)
            {
                byte[] userData = Encoding.ASCII.GetBytes(userInfo);
                _clientSocket.Send(userData);
            }
        }

        private string ReciveInformationFromServer()
        {
            try
            {
                byte[] reciveBuffer = new byte[1024];
                int rec = _clientSocket.Receive(reciveBuffer);
                byte[] data = new byte[rec];
                Array.Copy(reciveBuffer, data, rec);
                string returnFormServer = Encoding.ASCII.GetString(data);
                return returnFormServer;
            }
            catch (Exception e)
            {
                //LBserverStatus.Text = "not ok";
            }
            return "";
        }


        public void get_statistic()
        {

            string GetRoomsMessage = @"I0017{""Itamar"": ""Ori""}";
            SendInfrmaionToServer(GetRoomsMessage);
            string answer = ReciveInformationFromServer();

            answer = answer.Substring(5, answer.Length - 5);

            PersonalStats personalStats = System.Text.Json.JsonSerializer.Deserialize<PersonalStats>(answer);

            
            txtAvarageTime.Text += personalStats.statistics[3].Substring(0,4);
            txtCorrectAnswers.Text += personalStats.statistics[2];
            txtGamesPlayed.Text += personalStats.statistics[0];
            txtQuestionsAnswerd.Text += personalStats.statistics[1];

        }

        private void LogOutBtns_Checked(object sender, RoutedEventArgs e)
        {
            string LogoutMessage = @"D0017{""Itamar"": ""Ori""}";
            SendInfrmaionToServer(LogoutMessage);
            string answer = ReciveInformationFromServer();

            LoginWindow wnd = new LoginWindow(_clientSocket);
            wnd.Top = this.Top;
            wnd.Left = this.Left;
            wnd.Show();
            this.Hide();
        }
        
        private void HomeBtn_Checked(object sender, RoutedEventArgs e)
        {
            MenuWindow wnd = new MenuWindow(_clientSocket, userName);
            wnd.Top = this.Top;
            wnd.Left = this.Left;
            wnd.Show();
            this.Hide();
        }

        private void JoinRoomBtn_Checked(object sender, RoutedEventArgs e)
        {
            JoinRoomWindow wnd = new JoinRoomWindow(_clientSocket, userName);
            wnd.Top = this.Top;
            wnd.Left = this.Left;
            wnd.Show();
            this.Hide();
        }

        private void LeaderboardBtn_Checked(object sender, RoutedEventArgs e)
        {
            LeaderboardWindow wnd = new LeaderboardWindow(_clientSocket, userName);
            wnd.Top = this.Top;
            wnd.Left = this.Left;
            wnd.Show();
            this.Hide();
        }

        private void CreditsBtn_Checked(object sender, RoutedEventArgs e)
        {
            CreditsWindow wnd = new CreditsWindow(_clientSocket, userName);
            wnd.Top = this.Top;
            wnd.Left = this.Left;
            wnd.Show();
            this.Hide();
        }

        protected override void OnMouseLeftButtonDown(MouseButtonEventArgs e)
        {
            base.OnMouseLeftButtonDown(e);
            DragMove();
        }

        private void CreateRoomBtn_Checked(object sender, RoutedEventArgs e)
        {
            CreateRoomWindow wnd = new CreateRoomWindow(_clientSocket, userName);
            wnd.Top = this.Top;
            wnd.Left = this.Left;
            wnd.Show();
            this.Hide();
        }
    }
}