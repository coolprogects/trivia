﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Net.Sockets;
using System.Text.Json;
using System.Windows.Navigation;

namespace client_WPF_
{
    /// <summary>
    /// Interaction logic for LeaderboardWindow.xaml
    /// </summary>
    public partial class LeaderboardWindow : Window
    {
        private Socket _clientSocket = null;
        string userName;

        public LeaderboardWindow(Socket clientSocket, string username)
        {
            InitializeComponent();
            _clientSocket = clientSocket;
            userName = username;
            this.Title.Text = "Hello " + username;
            get_Leaderbord();
        }

        private void SendInfrmaionToServer(string userInfo)
        {
            if (_clientSocket.Connected)
            {
                byte[] userData = Encoding.ASCII.GetBytes(userInfo);
                _clientSocket.Send(userData);
            }
        }

        private string ReciveInformationFromServer()
        {
            try
            {
                byte[] reciveBuffer = new byte[1024];
                int rec = _clientSocket.Receive(reciveBuffer);
                byte[] data = new byte[rec];
                Array.Copy(reciveBuffer, data, rec);
                string returnFormServer = Encoding.ASCII.GetString(data);
                return returnFormServer;
            }
            catch (Exception e)
            {
                //LBserverStatus.Text = "not ok";
            }
            return "";
        }

        public void get_Leaderbord()
        {

            string GetRoomsMessage = @"J0017{""Itamar"": ""Ori""}";
            SendInfrmaionToServer(GetRoomsMessage);
            string answer = ReciveInformationFromServer();

            answer = answer.Substring(5, answer.Length - 5);

            LeaderBord leaders = System.Text.Json.JsonSerializer.Deserialize<LeaderBord>(answer);

            int[] xp = new int[5];
            string[] name = new string[5];
            double[] rate = new double[5];

            for (int i = 0; i < 5; i++)
            {
                int max = -1;
                int index = 1;
                for (int j = 1; j < leaders.leaderboard.Length; j = j + 3)
                {
                    if (int.Parse(leaders.leaderboard[j]) > max)
                    {
                        max = int.Parse(leaders.leaderboard[j]);
                        index = j;
                    }
                }

                leaders.leaderboard[index] = "-1";
                xp[i] = max;
                name[i] = leaders.leaderboard[index - 1];
                rate[i] = double.Parse(leaders.leaderboard[index + 1]);
            }



            if (leaders.leaderboard.Length >= 3) first.Text = "Name: " + name[0] + "\nxp: " + xp[0] + "\nsuccess rate: " + getPercent(rate[0].ToString());
            if (leaders.leaderboard.Length >= 6) second.Text = "Name: " + name[1] + "\nxp: " + xp[1] + "\nsuccess rate: " + getPercent(rate[1].ToString());
            if (leaders.leaderboard.Length >= 9) third.Text = "Name: " + name[2] + "\nxp: " + xp[2] + "\nsuccess rate: " + getPercent(rate[2].ToString());
            if (leaders.leaderboard.Length >= 12) fourth.Text = "Name: " + name[3] + ", xp: " + xp[3] + ", success rate: " + getPercent(rate[3].ToString());
            if (leaders.leaderboard.Length >= 15) fifth.Text = "Name: " + name[4] + ", xp: " + xp[4] + ", success rate: " + getPercent(rate[4].ToString());
        }

        private string getPercent(string rate)
        {
            if (rate == "0.0" || rate == "0")
            {
                return "0%";
            }
            if (rate == "1")
            {
                return "100%";
            }
            else return rate.Substring(2, 2) + "." + rate.Substring(2, 2) + "%";
        }

        private void JoinRoomBtn_Checked(object sender, RoutedEventArgs e)
        {
            JoinRoomWindow wnd = new JoinRoomWindow(_clientSocket, userName);
            wnd.Top = this.Top;
            wnd.Left = this.Left;
            wnd.Show();
            this.Hide();
        }

        private void CreateRoomBtn_Checked(object sender, RoutedEventArgs e)
        {
            CreateRoomWindow wnd = new CreateRoomWindow(_clientSocket, userName);
            wnd.Top = this.Top;
            wnd.Left = this.Left;
            wnd.Show();
            this.Hide();
        }

        protected override void OnMouseLeftButtonDown(MouseButtonEventArgs e)
        {
            base.OnMouseLeftButtonDown(e);
            DragMove();
        }

        private void LogOutBtns_Checked(object sender, RoutedEventArgs e)
        {
            string LogoutMessage = @"D0017{""Itamar"": ""Ori""}";
            SendInfrmaionToServer(LogoutMessage);
            string answer = ReciveInformationFromServer();

            LoginWindow wnd = new LoginWindow(_clientSocket);
            wnd.Top = this.Top;
            wnd.Left = this.Left;
            wnd.Show();
            this.Hide();
        }

        private void HomeBtn_Checked(object sender, RoutedEventArgs e)
        {
            MenuWindow wnd = new MenuWindow(_clientSocket, userName);
            wnd.Top = this.Top;
            wnd.Left = this.Left;
            wnd.Show();
            this.Hide();
        }

        private void JoinBtn_Click(object sender, RoutedEventArgs e)
        {
            JoinRoomWindow wnd = new JoinRoomWindow(_clientSocket, userName);
            wnd.Top = this.Top;
            wnd.Left = this.Left;
            wnd.Show();
            this.Hide();
        }

        private void StatisticsBtn_Checked(object sender, RoutedEventArgs e)
        {
            StatisticsWindow wnd = new StatisticsWindow(_clientSocket, userName);
            wnd.Top = this.Top;
            wnd.Left = this.Left;
            wnd.Show();
            this.Hide();
        }

        private void CreditsBtn_Checked(object sender, RoutedEventArgs e)
        {
            CreditsWindow wnd = new CreditsWindow(_clientSocket, userName);
            wnd.Top = this.Top;
            wnd.Left = this.Left;
            wnd.Show();
            this.Hide();
        }

    }

}
