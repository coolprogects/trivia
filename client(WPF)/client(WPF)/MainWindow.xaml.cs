﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using System.Text.Json;
using System.Net.Sockets;

using System.Net;

namespace client_WPF_
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        private static Socket _clientSocket = null;

        public MainWindow()
        {
            InitializeComponent();

            _clientSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            ConnectToServer();
        }

        private void ConnectToServer()
        {
            if (!_clientSocket.Connected)
            {
                try
                {
                    _clientSocket.Connect(IPAddress.Loopback, 55555);
                }
                catch (SocketException)
                {
                    //l2.Text = "conection state: not ok";
                }
                if (_clientSocket.Connected)
                {
                    //l2.Text = "conection state: ok";
                }
            }
        }

        private void Login_Click(object sender, RoutedEventArgs e)
        {
            this.Hide();
            LoginWindow wnd = new LoginWindow(_clientSocket);
            wnd.ShowDialog();
        }

        private void Signup_Click(object sender, RoutedEventArgs e)
        {
            this.Hide();
            SignupWindow wnd = new SignupWindow(_clientSocket);
            wnd.ShowDialog();
        }

        private void Exit_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
