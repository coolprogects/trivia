﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace client_WPF_
{

    class loginUser
    {
        public string username { set; get; }
        public string password { set; get; }

        public loginUser(string userName, string passWord)
        {
            username = userName;
            password = passWord;
        }
    }

    class signupUser
    {
        public string username { set; get; }
        public string password { set; get; }
        public string email { set; get; }

        public signupUser(string userName, string passWord, string Mail)
        {
            username = userName;
            password = passWord;
            email = Mail;
        }
    }

    class CreateRoom
    {
        public string roomName { set; get; }
        public int maxUsers { set; get; }
        public int questionCount { set; get; }
        public int answerTimeout { set; get; }

        public CreateRoom(string room_name, int max_users, int answer_timeout, int question_count)
        {
            roomName = room_name;
            maxUsers = max_users;
            questionCount = question_count;
            answerTimeout = answer_timeout;
        }

    }

    class JoinRoom
    {
        public string roomName { set; get; }
       
        public JoinRoom(string room_name)
        {
            roomName = room_name;
        }

    }

    public class RoomState
    {
        public int status { set; get; }
        public int hasGameBegun { set; get; }
        public string[] players { set; get; }
        public int questionCount { set; get; }
        public int answerTimeout { set; get; }
        

        public RoomState(int status, int hasGameBegun, string[] players, int questionCount, int answerTimeout)
        {
            this.status = status;
            this.hasGameBegun = hasGameBegun;
            this.players = players;
            this.questionCount = questionCount;
            this.answerTimeout = answerTimeout;
        }
    }

    class RoomList
    {
        public string[] rooms { set; get; }
        public int status { set; get; }

        public RoomList(int status, string[] rooms)
        {
            this.status = status;
            this.rooms = rooms;
        }
    }

    class PlayersList
    {
        public string[] players { set; get; }
        
        public PlayersList(string[] players)
        {
            this.players = players;
        }
    }

    class PersonalStats
    {
        
        public string[] statistics { set; get; }
        public int status { set; get; }

        public PersonalStats(string[] statistics, int status)
        {
            this.statistics = statistics;
            this.status = status;
        }

    }

    class LeaderBord
    {
        
        public string[] leaderboard { set; get; }

        public LeaderBord(string[] leaderboard)
        {
            this.leaderboard = leaderboard;
        }   
    }

    class submitRequest
    {

        public int answerId { set; get; }
        public int secondAmount { set; get; }

        public submitRequest(int answerId, int secondAmount)
        {
            this.answerId = answerId;
            this.secondAmount = secondAmount;
        }
    }

    class submitResponse
    {
        public int status { set; get; }
        public int correctAnswerId { set; get; }

        public submitResponse(int status, int correctAnswerId)
        {
            this.correctAnswerId = correctAnswerId;
            this.status = status;
        }
    }

}
