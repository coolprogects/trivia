﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using MaterialDesignThemes.Wpf;
using System.Windows.Shapes;
using System.Text.Json;
using System.Net.Sockets;


namespace client_WPF_
{
    /// <summary>
    /// Interaction logic for RegisterWindow.xaml
    /// </summary>

    public partial class SignupWindow : Window
    {

        enum codes {ERROR = 'A', LOGIN = 'C', SIGNUP = 'C' }
        private Socket _clientSocket = null;
        public bool IsDarkTheme { get; set; }
        private readonly PaletteHelper paletteHelper = new PaletteHelper();

        public SignupWindow(Socket clientSocket)
        {
            InitializeComponent();
            _clientSocket = clientSocket;
        }

        private void SendInfrmaionToServer(string userInfo)
        {
            if (_clientSocket.Connected)
            {
                // 1 convert the form informatino to byte array
                byte[] userData = Encoding.ASCII.GetBytes(userInfo);
                // send data to the server as byte array
                _clientSocket.Send(userData);
            }
        }

        private string ReciveInformationFromServer()
        {
            try
            {
                //_clientSocket.Connect(IPAddress.Loopback, 50000);
                //preper to recive data from the server
                //1.preper byte array to get all the bytes from the servr
                byte[] reciveBuffer = new byte[1024];
                //2.recive the data from the server in to the byte array and
                //return the size of bvtes how recive
                int rec = _clientSocket.Receive(reciveBuffer);
                //3. preper byte array with the size of bytes how recive frm
                //the servr
                byte[] data = new byte[rec];
                //4. copy the byte array how reive in to the byte array with
                //the correct size
                Array.Copy(reciveBuffer, data, rec);
                //5. convert the byte array to Ascii
                string returnFormServer = Encoding.ASCII.GetString(data);
                return returnFormServer;
            }
            catch (Exception e)
            {
                //LBserverStatus.Text = "not ok";
            }
            return "";
        }

        private void exitApp(object sender, RoutedEventArgs e)
        {
            Application.Current.Shutdown();
        }

        protected override void OnMouseLeftButtonDown(MouseButtonEventArgs e)
        {
            base.OnMouseLeftButtonDown(e);
            DragMove();
        }

        private void OnKeyDownHandler(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Return)
            {
                signupBtn_Click(sender, e);
            }
        }

        private void signupBtn_Click(object sender, RoutedEventArgs e)
        {
            signupUser newUser = new signupUser(this.txtUsername.Text, this.txtPassword.Password, this.txtMail.Text);
            string json = JsonSerializer.Serialize(newUser);
            string length = json.Length.ToString();
            string message;

            if (json.Length > 9999)
            {
                this.txtErrorMsg.Text = "The username and password are too long.";
                return;
            }
            while (length.Length < 4)
            {
                length = "0" + length;
            }

            message = (char)codes.SIGNUP + length + json;
            SendInfrmaionToServer(message);

            string answer = ReciveInformationFromServer();
            if (!(answer.Length != 0 && answer[0] == (char)codes.SIGNUP)) this.txtErrorMsg.Text = "The username is already taken,\nplease try again.";
            else
            {
                LoginWindow wnd = new LoginWindow(_clientSocket);
                wnd.Top = this.Top;
                wnd.Left = this.Left;
                wnd.Show();
                this.Hide();
            }

        }

        private void loginBtn_Click(object sender, RoutedEventArgs e)
        {
            LoginWindow wnd = new LoginWindow(_clientSocket);
            wnd.Top = this.Top;
            wnd.Left = this.Left;
            wnd.Show();
            this.Hide();
        }
    }
}
