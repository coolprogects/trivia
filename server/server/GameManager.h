#pragma once
#include "SqliteDatabase.h"
#include "Room.h"
#include "Game.h"
#include <vector>

class GameManager
{

public:
	GameManager();
	~GameManager();
	Game& createGame(Room room);
	void deleteGame(Game game);
	vector<Game*> getGames();

	bool isGameExist(LoggedUser user);
	Game& getGame(LoggedUser user);

	void gameResult(Game game, LoggedUser user);

private:
	IDatabase* _database;
	vector<Game*> _games;

};


