#include "RoomAdminRequestHandler.h"
#include "JsonResponsePacketSerializer.h"
#include "JsonRequestPacketDeserializer.h"

RoomAdminRequestHandler::RoomAdminRequestHandler(RequestHandlerFactory& handlerFactory, RoomManager& roomManager, Room& room, LoggedUser user) :
    _handlerFactory(handlerFactory), _roomManager(roomManager), _room(room)
{
    _user = user;
}

RoomAdminRequestHandler::~RoomAdminRequestHandler()
{
}

bool RoomAdminRequestHandler::isRequestRelevant(RequestInfo requestInfo)
{
	return requestInfo.id == CLOSE_ROOM_CODE || requestInfo.id == START_GAME_CODE || requestInfo.id == ROOM_STATE_CODE;
}

RequestResult RoomAdminRequestHandler::handleRequest(RequestInfo requestInfo)
{
    RequestResult result;

    switch (requestInfo.id)
    {

    case CLOSE_ROOM_CODE:
    {
        result = closeRoom(requestInfo);
        break;
    }

    case START_GAME_CODE:
    {
        result = startGame(requestInfo);
        break;
    }

    case ROOM_STATE_CODE:
    {
        result = getRoomState(requestInfo);
        break;
    }
    }
    return result;
}

RequestResult RoomAdminRequestHandler::closeRoom(RequestInfo requestInfo)
{
    
    CloseRoomResponse request
    {
        1
    };

    vector<unsigned char> buffer = JsonResponsePacketSerializer::serializeResponse(request);
    MenuRequestHandler* handler = _handlerFactory.createMenuRequestHandler(_user);
    
    _room.removeUser(_user);
    _room.unActivate();
    _roomManager.CleanRooms();

    RequestResult result
    {
        buffer,
        handler
    };

    return result;
}

RequestResult RoomAdminRequestHandler::startGame(RequestInfo requestInfo)
{
    Game game;

    StartGameResponse request
    {
        1
    };

    vector<unsigned char> buffer = JsonResponsePacketSerializer::serializeResponse(request);
    GameRequestHandler* handler = _handlerFactory.createGameRequestHandler(_user, _handlerFactory.getGameManager().createGame(_room));

    RequestResult result
    {
        buffer,
        handler
    };

    _room.activate();
    _room.removeUser(_user);
    _roomManager.CleanRooms();

    return result;
}

RequestResult RoomAdminRequestHandler::getRoomState(RequestInfo requestInfo)
{
    
    GetRoomStateResponse request
    {
        1,
        _room.getRoomData().isActive,
        _room.getAllUsers(),
        _room.getRoomData().numOfQuestionsInGame,
        _room.getRoomData().timePerQuestion
    };


    vector<unsigned char> buffer = JsonResponsePacketSerializer::serializeResponse(request);
    RoomAdminRequestHandler* handler = _handlerFactory.createRoomAdminRequestHandler(_room, _user);

    RequestResult result
    {
        buffer,
        handler
    };

	return result;
}
