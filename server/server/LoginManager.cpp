#include "LoginManager.h"

LoginManager::LoginManager()
{
	_database = new SqliteDatabase();
}

LoginManager::~LoginManager()
{
}

int LoginManager::signup(string username, string password, string email)
{
	if (_database->doesUserExist(username))
	{
		return 0;
	}
	else
	{
		_database->addNewUser(username, password, email);
		return 1;
	}
}

int LoginManager::login(string username, string password)
{
	if (!_database->doesUserExist(username) || !_database->doesPasswordMatch(username, password) || find(_loggedUsers.begin(), _loggedUsers.end(), LoggedUser(username)) != _loggedUsers.end())
	{
		return 0;
	}
	else
	{
		_loggedUsers.push_back(LoggedUser(username));
		return 1;
	}
}

int LoginManager::logout(string username)
{
	if (find(_loggedUsers.begin(), _loggedUsers.end(), LoggedUser(username)) == _loggedUsers.end())
	{
		return 0;
	}
	else
	{
		_loggedUsers.erase(std::remove(_loggedUsers.begin(), _loggedUsers.end(), LoggedUser(username)), _loggedUsers.end());
		return 1;
	}
}