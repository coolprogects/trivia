#pragma comment (lib, "ws2_32.lib")
#include "WSAInitializer.h"
#include "Server.h"
#include <iostream>
#include <fstream>

using std::exception;

int main()
{
	try
	{
		cout << "Starting..." << endl;
		WSAInitializer wsa_init;
		Server md_server;
		md_server.run();
	}
	catch (const exception& e)
	{
		std::cout << "Exception was thrown in function: " << e.what() << std::endl;
	}
	catch (...)
	{
		std::cout << "Unknown exception in main !" << std::endl;
	}
}
