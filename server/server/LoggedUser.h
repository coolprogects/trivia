#pragma once
#include <string>

using std::string;

class LoggedUser
{

public:
	LoggedUser();
	LoggedUser(string username);
	string getUsername() const;

	// For the vector
	bool operator==(const LoggedUser& other) const;
	bool operator <(const LoggedUser& other) const;
	bool operator >(const LoggedUser& other) const;

private:
	string _username;

};