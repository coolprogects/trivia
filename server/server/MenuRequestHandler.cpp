#include "Requests.h"
#include "MenuRequestHandler.h"
#include "JsonResponsePacketSerializer.h"
#include "JsonRequestPacketDeserializer.h"

MenuRequestHandler::MenuRequestHandler(RoomManager& roomManager, StatisticsManager& statisticsManager, RequestHandlerFactory& handlerFactory, LoggedUser user) : _roomManager(roomManager), _statisticsManager(statisticsManager), _handlerFactory(handlerFactory)
{
    this->_user = user;
}

MenuRequestHandler::~MenuRequestHandler()
{
}

bool MenuRequestHandler::isRequestRelevant(RequestInfo requestInfo)
{
    return requestInfo.id >= LOG_OUT_CODE && requestInfo.id <= GET_LEADERBORD_CODE;
}

RequestResult MenuRequestHandler::handleRequest(RequestInfo requestInfo)
{
    RequestResult result;

    switch (requestInfo.id)
    {

    case LOG_OUT_CODE:
    {
        result = logout(requestInfo);
        break;
    }

    case GET_ROOMS_CODE:
    {
        result = getRooms(requestInfo);
        break;
    }

    case GET_PLAYERS_IN_ROOM_CODE:
    {
        result = getPlayersInRoom(requestInfo);
        break;
    }

    case JOIN_ROOM_CODE:
    {
        result = joinRoom(requestInfo);
        break;
    }

    case CREATE_ROOM_CODE:
    {
        result = createRoom(requestInfo);
        break;
    }

    case GET_PERSONAL_STATS_CODE:
    {
        result = getPersonalStats(requestInfo);
        break;
    }

    case GET_LEADERBORD_CODE:
    {
        result = getHighScore(requestInfo);
        break;
    }

    }

    return result;
}

RequestResult MenuRequestHandler::logout(RequestInfo requestInfo)
{
    int success = _handlerFactory.getLoginManager().logout(_user.getUsername());

    GetRoomsResponse response
    {
        success
    };

    vector<unsigned char> buffer = JsonResponsePacketSerializer::serializeResponse(response);
    LoginRequestHandler* handler = _handlerFactory.createLoginRequestHandler();

    RequestResult result
    {
        buffer,
        handler,
    };

    return result;
}

RequestResult MenuRequestHandler::getRooms(RequestInfo requestInfo)
{
    vector<RoomData> rooms = _roomManager.getRooms();

    GetRoomsResponse response
    {
        rooms.size() > 0,
        rooms
    };

    vector<unsigned char> buffer = JsonResponsePacketSerializer::serializeResponse(response);
    MenuRequestHandler* handler = _handlerFactory.createMenuRequestHandler(_user);

    RequestResult result
    {
        buffer,
        handler,
    };

    return result;
}

RequestResult MenuRequestHandler::getPlayersInRoom(RequestInfo requestInfo)
{
    GetPlayersInRoomRequest request = JsonRequestPacketDeserializer::deserializeGetPlayersRequest(requestInfo.buffer);
    vector<string> playersInRoom = _roomManager.getRoom(request.roomName).getAllUsers();

    GetPlayersInRoomResponse response
    {
        playersInRoom,
    };

    vector<unsigned char> buffer = JsonResponsePacketSerializer::serializeResponse(response);
    MenuRequestHandler* handler = _handlerFactory.createMenuRequestHandler(_user);

    RequestResult result
    {
        buffer,
        handler,
    };

    return result;
}

RequestResult MenuRequestHandler::getPersonalStats(RequestInfo requestInfo)
{
    vector<string> personalStats = _statisticsManager.getUserStatistics(_user.getUsername());
    GetPersonalStatsResponse response
    {
        1,
        personalStats
    };

    vector<unsigned char> buffer = JsonResponsePacketSerializer::serializeResponse(response);
    MenuRequestHandler* handler = _handlerFactory.createMenuRequestHandler(_user);

    RequestResult result
    {
        buffer,
        handler,
    };

    return result;
}

RequestResult MenuRequestHandler::getHighScore(RequestInfo requestInfo)
{
    vector<string> highScores = _statisticsManager.getHighScore();
    GetHighScoreResponse response
    {
        1,
        highScores
    };

    vector<unsigned char> buffer = JsonResponsePacketSerializer::serializeResponse(response);
    MenuRequestHandler* handler = _handlerFactory.createMenuRequestHandler(_user);

    RequestResult result
    {
        buffer,
        handler,
    };

    return result;
}

RequestResult MenuRequestHandler::joinRoom(RequestInfo requestInfo)
{
    JoinRoomRequest request = JsonRequestPacketDeserializer::deserializeJoinRoomRequest(requestInfo.buffer);
    int success = _roomManager.getRoom(request.roomName).addUser(_user);


    JoinRoomResponse response
    {
        success
    };

    vector<unsigned char> buffer = JsonResponsePacketSerializer::serializeResponse(response);

    RequestResult result
    {
        buffer,
    };

    if (success) result.newHandler = _handlerFactory.createRoomMemberRequestHandler(_roomManager.getRoom(request.roomName), _user);
    else result.newHandler = _handlerFactory.createMenuRequestHandler(_user);


    return result;
}

RequestResult MenuRequestHandler::createRoom(RequestInfo requestInfo)
{
    CreateRoomRequest request = JsonRequestPacketDeserializer::deserializeCreateRoomRequest(requestInfo.buffer);
    
    
    RoomData roomData
    {
        _roomManager.getNewId(),
        request.roomName,
        request.maxUsers,
        request.questionCount,
        request.answerTimeout,
        0
    };

    int success = _roomManager.canCreate(_user, roomData);

    CreateRoomResponse response
    {
        success
    };

    vector<unsigned char> buffer = JsonResponsePacketSerializer::serializeResponse(response);
    //auto handler;

    RequestResult result
    {
        buffer,
    };

    if(success) result.newHandler = _handlerFactory.createRoomAdminRequestHandler(_roomManager.createRoom(_user, roomData), _user);
    else result.newHandler = _handlerFactory.createMenuRequestHandler(_user);

    return result;
}
