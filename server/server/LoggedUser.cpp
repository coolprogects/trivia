#include "LoggedUser.h"

LoggedUser::LoggedUser()
{
	_username = "itamarAlush";
}

LoggedUser::LoggedUser(string username)
{
	_username = username;
}

string LoggedUser::getUsername() const
{
	return _username;
}

bool LoggedUser::operator<(const LoggedUser& other) const
{
	return this->_username < other._username;
}

bool LoggedUser::operator>(const LoggedUser& other) const
{
	return this->_username  > other._username;
}

bool LoggedUser::operator==(const LoggedUser& other) const
{
	return this->_username == other._username;
}
