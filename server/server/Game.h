#pragma once
#include <map>
#include "Room.h"
#include "Question.h"
#include "LoggedUser.h"

using std::map;

typedef struct GameData
{
	Question currentQuestion;
	unsigned int correctAnswerCount;
	unsigned int wrongAnswerCount;
	unsigned int averangeAnswerTime;

	inline bool operator<(GameData other) const
	{
		return correctAnswerCount < other.correctAnswerCount;
	}
	inline bool operator>(GameData other) const
	{
		return correctAnswerCount > other.correctAnswerCount;
	}
	inline bool operator==(GameData other) const
	{
		return correctAnswerCount == other.correctAnswerCount && wrongAnswerCount == other.wrongAnswerCount && averangeAnswerTime == other.averangeAnswerTime && currentQuestion == other.currentQuestion;
	}

}GameData;

class Game
{

public:
	Game(Room room);
	Game() = default;

	Question getQuestionForUser(LoggedUser user);
	void submitAnswer(LoggedUser user, unsigned int answerID, unsigned int seconds);
	void removePlayer(LoggedUser user);
	bool isGameOver(LoggedUser user);

	map<LoggedUser, GameData> getPlayers();
	vector<Question> getQuestions();

	// For the vector
	bool operator==(const Game& other) const;
	bool operator <(const Game& other) const;
	bool operator >(const Game& other) const;
	
	bool IsPlayerExist(LoggedUser user);
	void leaveGame();
	bool gameEnded();

private:
	vector<Question> _questions;
	map<LoggedUser, GameData> _players;
	int playersAmount;
};

