#pragma once
#include <string>
#include <vector>

using std::string;
using std::vector;

typedef struct LoginRequest
{
	string username;
	string password;
}LoginRequest;

typedef struct SignupRequest
{
	string username;
	string password;
	string email;
}SignupRequest;

typedef struct GetPlayersInRoomRequest
{
	string roomName;
}GetPlayersInRoomRequest;

typedef struct JoinRoomRequest
{
	string roomName;
}JoinRoomRequest;


typedef struct CreateRoomRequest
{
	string roomName;
	unsigned int maxUsers;
	unsigned int questionCount;
	unsigned int answerTimeout;
}CreateRoomRequest;

typedef struct SubmitAnswerRequest
{
	unsigned int answerId;
	unsigned int secondAmount;
}SubmitAnswerRequest;

