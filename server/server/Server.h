#pragma once
#include "IDatabase.h"
#include "Communicator.h"

#define EXIT_COMMAND "EXIT"

class Server
{

public:
	void run();

private:
	void runCommunicator();

	IDatabase* _database;
	RequestHandlerFactory _handlerFactory;
};

