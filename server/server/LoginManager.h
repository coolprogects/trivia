#pragma once
#include "LoggedUser.h"
#include "SqliteDatabase.h"
#include <vector>

class LoginManager
{

public:
	LoginManager();
	~LoginManager();

	int signup(string username, string password, string email);
	int login(string username, string password);
	int logout(string username);

private:
	IDatabase* _database;
	vector<LoggedUser> _loggedUsers;

};