#pragma once
#include <map>
#include <deque>
#include <queue>
#include <mutex>
#include <iostream>
#include <WinSock2.h>
#include <condition_variable>
#include "RequestHandlerFactory.h"

using std::map;
using std::endl;
using std::cout;

class Communicator
{

public:
	Communicator(RequestHandlerFactory& handlerFactory);
	~Communicator();

	void startHandleRequests();
	static void sendToPlayers(vector<string>,  RequestResult result);
private:
	SOCKET _serverSocket;
	map<SOCKET, IRequestHandler*> _clients;
	RequestHandlerFactory& _handlerFactory;
	static map<string, SOCKET> _nameToSocket;

	void acceptClient();
	void bindAndListen();
	void handleNewClient(const SOCKET client_socket);

	//solution for problem.
	void addClient(string message, string response, SOCKET clientSocket);
	void deleteUser(SOCKET client_socket);
	RequestResult execution(SOCKET clientSocket, RequestInfo requestInfo);
	
	//void display_count();
};