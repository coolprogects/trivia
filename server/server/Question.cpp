#include "Question.h"

Question::Question(string question, string correctAnswer, vector<string> possibleAnswers)
{
    _question = question;
    _correctAnswer = correctAnswer;
    _possibleAnswers = possibleAnswers;
}


string Question::getQuestion()
{
    return _question;
}

vector<string> Question::getPossibleAnswers()
{
    return _possibleAnswers;
}

string Question::getCorrectAnswer()
{
    return _correctAnswer;
}

bool Question::operator==(const Question& other) const
{
    return this->_question == other._question && other._correctAnswer == this->_correctAnswer;
}

bool Question::operator<(const Question& other) const
{
    return _possibleAnswers.size() < other._possibleAnswers.size();
}

bool Question::operator>(const Question& other) const
{
    return _possibleAnswers.size() > other._possibleAnswers.size();;
}
