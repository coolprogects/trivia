#pragma once

#include "Room.h"
#include "LoggedUser.h"
#include "RequestResult.h"
#include "IRequestHandler.h"
#include "RequestHandlerFactory.h"

#define CLOSE_ROOM_CODE 'K'
#define START_GAME_CODE 'M'
#define ROOM_STATE_CODE 'L'

class RequestHandlerFactory;

class RoomAdminRequestHandler : public IRequestHandler
{

public:
	RoomAdminRequestHandler(RequestHandlerFactory& handlerFactory, RoomManager& roomManager, Room& room, LoggedUser user);
	~RoomAdminRequestHandler();
	
	virtual bool isRequestRelevant(RequestInfo requestInfo) override;
	virtual RequestResult handleRequest(RequestInfo requestInfo) override;

private:
	Room& _room;
	LoggedUser _user;
	RoomManager& _roomManager;
	RequestHandlerFactory& _handlerFactory;

	RequestResult closeRoom(RequestInfo requestInfo);
	RequestResult startGame(RequestInfo requestInfo);
	RequestResult getRoomState(RequestInfo requestInfo);

};

