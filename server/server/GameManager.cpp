#include "GameManager.h"

using std::remove;

GameManager::GameManager()
{
    _database = new SqliteDatabase();
}

GameManager::~GameManager()
{
}

Game& GameManager::createGame(Room room)
{
    Game* game  = new Game(room);
    _games.push_back(game);
    return *game;
}

void GameManager::deleteGame(Game game)
{
    for (auto it = _games.begin(); it != _games.end(); it++)
    {
        if ((*it)->gameEnded())
        {
            _games.erase(it);
            break;
        }
    }       
}

vector<Game*> GameManager::getGames()
{
    return _games;
}

bool GameManager::isGameExist(LoggedUser user)
{
    for (auto game : _games)
    {
        if ((*game).IsPlayerExist(user)) return true;
    }

    return false;
}

Game& GameManager::getGame(LoggedUser user)
{
    for (auto game : _games)
    {
        if ((*game).IsPlayerExist(user)) return *game;
    }
    
}

void GameManager::gameResult(Game game, LoggedUser user)
{
    GameData data = game.getPlayers()[user];
    _database->updateGameResult(user.getUsername(), data.correctAnswerCount, game.getQuestions().size() ,data.averangeAnswerTime);
}
