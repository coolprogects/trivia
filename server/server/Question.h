#pragma once
#include <string>
#include <vector>

using std::string;
using std::vector;

class Question
{

public:
	Question(string question, string correctAnswer, vector<string> possibleAnswers);

	Question() = default;

	string getQuestion();
	vector<string> getPossibleAnswers();
	string getCorrectAnswer();

	// For the vector
	bool operator==(const Question& other) const;
	bool operator <(const Question& other) const;
	bool operator >(const Question& other) const;

private:
	string _question;
	string _correctAnswer;
	vector<string> _possibleAnswers;

};

