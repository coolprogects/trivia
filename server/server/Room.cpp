#include "Room.h"

Room::Room(LoggedUser user, RoomData roomData)
{
	_metadata = roomData;
	addUser(user);
}

Room::~Room()
{
}

unsigned int Room::addUser(LoggedUser user)
{
	if (_metadata.maxPlayers > _users.size())
	{
		_users.push_back(user);
		return 1;
	}
	else
	{
		return 0;
	}
}

void Room::removeUser(LoggedUser user)
{
	_users.erase(std::find(_users.begin(), _users.end(), user.getUsername()));	
}

vector<string> Room::getAllUsers()
{
	vector<string> usersString;

	for (auto user : _users)
	{
		usersString.push_back(user.getUsername());
	}

	return usersString;
}

RoomData Room::getRoomData()
{
	return _metadata;
}

void Room::activate()
{
	_metadata.isActive = 1;
}

void Room::unActivate()
{
	_metadata.isActive = 2;
}



