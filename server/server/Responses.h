#pragma once
#include <map>
#include <string>
#include "Room.h"

using std::map;
using std::string;

typedef struct LoginResponse
{
	unsigned int status;
}LoginResponse;

typedef struct SignupResponse
{
	unsigned int status;
}SignupResponse;

typedef struct ErrorResponse
{
	string message;
}ErrorResponse;

typedef struct LogoutResponse
{
	unsigned int status;
}LogoutResponse;

typedef struct GetRoomsResponse
{
	unsigned int status;
	vector<RoomData> rooms;
}GetRoomsResponse;

typedef struct GetPlayersInRoomResponse
{
	vector<string> players;
}GetPlayersInRoomResponse;

typedef struct GetHighScoreResponse
{
	unsigned int status;
	vector<string> leaderboard;
}GetHighScoreResponse;

typedef struct GetPersonalStatsResponse
{
	unsigned int status;
	vector<string> statistics;
}GetPersonalStatsResponse;

typedef struct JoinRoomResponse
{
	unsigned int status;
}JoinRoomResponse;

typedef struct CreateRoomResponse
{
	unsigned int status;
}CreateRoomResponse;

typedef struct CloseRoomResponse
{
	unsigned int status;
}CloseRoomResponse;

typedef struct StartGameResponse
{
	unsigned int status;
}StartGameResponse;

typedef struct GetRoomStateResponse
{
	unsigned int status;
	int hasGameBegun;
	vector<string> players;
	unsigned int questionCount;
	unsigned int answerTimeout;
}GetRoomStateResponse;

typedef struct LeaveRoomResponse
{
	unsigned int status;
}LeaveRoomResponse;

typedef struct LeaveGameResponse
{
	unsigned int status;
}LeaveGameResponse;

typedef struct GetQuestionResponse
{
	unsigned int status;
	string question;
	map<unsigned int, string> answers;
}GetQuestionResponse;

typedef struct SubmitAnswerResponse
{
	unsigned int status;
	unsigned int correctAnswerId;
}SubmitAnswerResponse;

typedef struct PlayerResults
{
	string username;
	float correctAnswerCount;
	float wrongAnswerCount;
	float averageAnswerTime;
}PlayerResults;

typedef struct GetGameResultsResponse
{
	unsigned int status;
	vector<PlayerResults> results;

}GetGameResultsResponse;