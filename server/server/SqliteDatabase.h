#pragma once
#include <list>
#include <vector>
#include <iostream>
#include "io.h"
#include "sqlite3.h"
#include "IDatabase.h"

#define NUM_OF_QUESTIONS 10

using std::list;
using std::vector;

class SqliteDatabase : public IDatabase
{
public:
	SqliteDatabase();
	~SqliteDatabase();

	virtual bool doesUserExist(string username) override;
	virtual bool doesPasswordMatch(string username, string password) override;
	virtual void addNewUser(string username, string password, string email) override; //Need to add user also to statistic tabel.

	virtual float getPlayerAverageAnswerTime(string username) override;
	virtual int getNumOfCorrectAnswers(string username) override;
	virtual int getNumOfTotalAnswers(string username) override;
	virtual int getNumOfPlayerGames(string username) override;

	virtual vector<Question> getQuestions(int amount) override;

	virtual vector<string> getAllUsers() override; // our function
	virtual void updateGameResult(string username, int correctAnswerCount, int questionsAmount, int AnswerTimeAmount) override; // our function

private:
	sqlite3* _db;

	void addQuestions();
	int excecuteSql(string sqlStatement, int (*callback)(void*, int, char**, char**));
	static vector<string> _values;
	static int callback(void* NotUsed, int argc, char** argv, char** azColName);
};