#pragma once
#include "RoomManager.h"
#include "LoginManager.h"
#include "StatisticsManager.h"
#include "GameManager.h"

#include "LoginRequestHandler.h"
#include "MenuRequestHandler.h"
#include "RoomAdminRequestHandler.h"
#include "RoomMemberRequestHandler.h"
#include "GameRequestHandler.h"

class LoginRequestHandler;
class MenuRequestHandler;
class RoomAdminRequestHandler;
class RoomMemberRequestHandler;
class GameRequestHandler;

class RequestHandlerFactory
{

public:
	LoginRequestHandler* createLoginRequestHandler();
	LoginManager& getLoginManager();

	MenuRequestHandler* createMenuRequestHandler(LoggedUser user);
	StatisticsManager& getStatisticsManager();
	RoomManager& getRoomManager();

	RoomAdminRequestHandler* createRoomAdminRequestHandler(Room& room, LoggedUser user);
	RoomMemberRequestHandler* createRoomMemberRequestHandler(Room& room, LoggedUser user);
	
	GameRequestHandler* createGameRequestHandler(LoggedUser user, Game& game);
	GameManager& getGameManager();

private:
	IDatabase* _database;
	RoomManager _roomManager;
	LoginManager _loginManager;
	StatisticsManager _statisticsManager;
	GameManager _gameManager;
};