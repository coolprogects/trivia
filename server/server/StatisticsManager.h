#pragma once
#include <map>
#include <string>
#include <vector>
#include <algorithm>
#include <iostream>
#include "SqliteDatabase.h"

#define NUM_OF_HIGH_SCORES 5

using std::map;
using std::copy;
using std::sort;
using std::pair;
using std::string;
using std::vector;
using std::to_string;
using std::back_inserter;

class StatisticsManager
{

public:
	StatisticsManager();
	~StatisticsManager();

	vector<string> getHighScore();
	vector<string> getUserStatistics(string username);

private:
	IDatabase* _database;
};

