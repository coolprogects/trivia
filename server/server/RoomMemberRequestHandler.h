#pragma once
#include "IRequestHandler.h"
#include "Room.h"
#include "LoggedUser.h"
#include "RequestResult.h"
#include "IRequestHandler.h"
#include "RequestHandlerFactory.h"

#define ROOM_STATE_CODE 'L' 
#define LEAVE_ROOM_CODE 'N'


class RoomMemberRequestHandler : public IRequestHandler
{

public:
	
	RoomMemberRequestHandler(RequestHandlerFactory& handlerFactory, RoomManager& roomManager, Room& room, LoggedUser user);
	~RoomMemberRequestHandler();

	virtual bool isRequestRelevant(RequestInfo requestInfo);
	virtual RequestResult handleRequest(RequestInfo requestInfo);

private:
	Room& _room;
	LoggedUser _user;
	RoomManager& _roomManager;
	RequestHandlerFactory& _handlerFactory;

	RequestResult leaveRoom(RequestInfo requestInfo);
	RequestResult getRoomState(RequestInfo requestInfo);

};

