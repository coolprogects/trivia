#include "Server.h"

using std::cin;

void Server::run()
{
	string input;

	std::thread t1(&Server::runCommunicator, this);
	t1.detach();

	do
	{
		cin >> input;
	} while (input != EXIT_COMMAND);
}

void Server::runCommunicator()
{
	_database = new SqliteDatabase();
	Communicator communicator = Communicator(_handlerFactory);
	communicator.startHandleRequests();
}
