#include <iostream>
#include "Requests.h"
#include "LoginRequestHandler.h"
#include "JsonResponsePacketSerializer.h"
#include "JsonRequestPacketDeserializer.h"

LoginRequestHandler::LoginRequestHandler(LoginManager& loginManager, RequestHandlerFactory& handlerFactory)
	: _loginManager(loginManager), _handlerFactory(handlerFactory)
{
}


LoginRequestHandler::~LoginRequestHandler()
{
}

bool LoginRequestHandler::isRequestRelevant(RequestInfo requestInfo)
{
	return LOGIN_CODE == requestInfo.id || SIGN_UP_CODE == requestInfo.id;
}

RequestResult LoginRequestHandler::handleRequest(RequestInfo requestInfo)
{
	RequestResult result;

	switch (requestInfo.id)
	{

	case LOGIN_CODE:
	{
		result = login(requestInfo);
		break;
	}

	case SIGN_UP_CODE:
	{
		result = signup(requestInfo);
		break;
	}

	}

	return result;
}

RequestResult LoginRequestHandler::error(RequestInfo requestInfo)
{
	ErrorResponse response
	{
		"ERROR"
	};

	vector<unsigned char> buffer = JsonResponsePacketSerializer::serializeResponse(response);
	LoginRequestHandler* handler = _handlerFactory.createLoginRequestHandler();

	RequestResult result
	{
		buffer,
		handler
	};

	return result;
}

RequestResult LoginRequestHandler::login(RequestInfo requestInfo)
{
	LoginRequest request = JsonRequestPacketDeserializer::deserializeLoginRequest(requestInfo.buffer);
	int success = _loginManager.login(request.username, request.password);

	if (!success) // Error response
	{
		return error(requestInfo);
	}

	LoginResponse response
	{
		success
	};

	vector<unsigned char> buffer = JsonResponsePacketSerializer::serializeResponse(response);
	MenuRequestHandler* handler = _handlerFactory.createMenuRequestHandler(LoggedUser(request.username));

	

	RequestResult result
	{
		buffer,
		handler
	};

	return result;
}

RequestResult LoginRequestHandler::signup(RequestInfo requestInfo)
{
	SignupRequest request = JsonRequestPacketDeserializer::deserializeSignupRequest(requestInfo.buffer);
	int success = _loginManager.signup(request.username, request.password, request.email);

	if (!success) // Error response
	{
		return error(requestInfo);
	}

	SignupResponse response
	{
		success
	};

	vector<unsigned char> buffer = JsonResponsePacketSerializer::serializeResponse(response);
	LoginRequestHandler* handler = _handlerFactory.createLoginRequestHandler();
	RequestResult result
	{
		buffer,
		handler
	};

	return result;
}
