#pragma once
#include "Requests.h"
#include <nlohmann/json.hpp>
#include <vector>

using nlohmann::json;
using std::vector;

class JsonRequestPacketDeserializer
{

public:

	static LoginRequest deserializeLoginRequest(vector<unsigned char> buffer);
	static SignupRequest deserializeSignupRequest(vector<unsigned char> buffer);

	static GetPlayersInRoomRequest deserializeGetPlayersRequest(vector<unsigned char> buffer);
	static JoinRoomRequest deserializeJoinRoomRequest(vector<unsigned char> buffer);
	static CreateRoomRequest deserializeCreateRoomRequest(vector<unsigned char> buffer);

	static SubmitAnswerRequest deserializerSubmitAnswerRequest(vector<unsigned char> buffer);
};

