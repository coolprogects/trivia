#pragma once
#include "LoggedUser.h"
#include <string>
#include <vector>

using std::string;
using std::vector;

typedef struct RoomData
{

	unsigned int id;
	string name;
	unsigned int maxPlayers;
	unsigned int numOfQuestionsInGame;
	unsigned int timePerQuestion;
	unsigned int isActive;

}RoomData;

class Room
{

public:
	Room(LoggedUser user, RoomData roomData);
	
	Room() = default;
	~Room();

	unsigned int addUser(LoggedUser user);
	void removeUser(LoggedUser user);
	vector<string> getAllUsers();
	
	RoomData getRoomData();

	// OUR FUNCTION !!!
	void activate();
	void unActivate();

private:
	RoomData _metadata;
	vector<LoggedUser> _users;

};

