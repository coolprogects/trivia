#pragma once
#include <vector>
#include "IRequestHandler.h"

using std::vector;

typedef struct RequestResult
{
	vector<unsigned char> buffer;
	IRequestHandler* newHandler;
}RequestResult;