#include "RequestHandlerFactory.h"

LoginRequestHandler* RequestHandlerFactory::createLoginRequestHandler()
{
    LoginRequestHandler* loginRequestHandler = new LoginRequestHandler(getLoginManager(), *this);
    return loginRequestHandler;
}

LoginManager& RequestHandlerFactory::getLoginManager()
{
    return _loginManager;
}

MenuRequestHandler* RequestHandlerFactory::createMenuRequestHandler(LoggedUser user)
{
    return new MenuRequestHandler(getRoomManager(), getStatisticsManager(), *this, user);
}

StatisticsManager& RequestHandlerFactory::getStatisticsManager()
{
    return _statisticsManager;
}

RoomManager& RequestHandlerFactory::getRoomManager()
{
    return _roomManager;
}

RoomAdminRequestHandler* RequestHandlerFactory::createRoomAdminRequestHandler(Room& room, LoggedUser user)
{
    return new RoomAdminRequestHandler(*this, getRoomManager(), room, user);
}

RoomMemberRequestHandler* RequestHandlerFactory::createRoomMemberRequestHandler(Room& room, LoggedUser user)
{
    return new RoomMemberRequestHandler(*this, getRoomManager(), room, user);
}

GameRequestHandler* RequestHandlerFactory::createGameRequestHandler(LoggedUser user, Game& game)
{     
    return new GameRequestHandler(*this, getGameManager(), user, game);
}

GameManager& RequestHandlerFactory::getGameManager()
{
    return this->_gameManager;
}
