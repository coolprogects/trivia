#pragma once
#include "Room.h"
#include "RequestResult.h"
#include "IRequestHandler.h"
#include "RequestHandlerFactory.h"

#define LOG_OUT_CODE 'D'
#define GET_ROOMS_CODE 'E'
#define GET_PLAYERS_IN_ROOM_CODE 'F'
#define JOIN_ROOM_CODE 'G'
#define CREATE_ROOM_CODE 'H'
#define GET_PERSONAL_STATS_CODE 'I' 
#define GET_LEADERBORD_CODE 'J'

class RequestHandlerFactory;

class MenuRequestHandler : public IRequestHandler
{
public:
	MenuRequestHandler(RoomManager& roomManager, StatisticsManager& statisticsManager, RequestHandlerFactory& handlerFactory, LoggedUser user);
	~MenuRequestHandler();

	virtual bool isRequestRelevant(RequestInfo requestInfo) override;
	virtual RequestResult handleRequest(RequestInfo requestInfo) override;

private:
	LoggedUser _user;
	RoomManager& _roomManager;
	StatisticsManager& _statisticsManager;
	RequestHandlerFactory& _handlerFactory;

	RequestResult logout(RequestInfo requestInfo);
	RequestResult getRooms(RequestInfo requestInfo);
	RequestResult getPlayersInRoom(RequestInfo requestInfo);
	RequestResult getPersonalStats(RequestInfo requestInfo);
	RequestResult getHighScore(RequestInfo requestInfo);
	RequestResult joinRoom(RequestInfo requestInfo);
	RequestResult createRoom(RequestInfo requestInfo);
};

