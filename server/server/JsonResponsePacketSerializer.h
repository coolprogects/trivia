#pragma once
#include <vector>
#include <nlohmann/json.hpp>
#include "Responses.h"

using nlohmann::json;
using std::vector;
using std::map;

class JsonResponsePacketSerializer
{

public:

	static vector<unsigned char> serializeResponse(ErrorResponse response);
	static vector<unsigned char> serializeResponse(LoginResponse response);
	static vector<unsigned char> serializeResponse(SignupResponse response);

	static vector<unsigned char> serializeResponse(LogoutResponse response);
	static vector<unsigned char> serializeResponse(GetRoomsResponse response);
	static vector<unsigned char> serializeResponse(GetPlayersInRoomResponse response);
	static vector<unsigned char> serializeResponse(JoinRoomResponse response);
	static vector<unsigned char> serializeResponse(CreateRoomResponse response);
	static vector<unsigned char> serializeResponse(GetHighScoreResponse response);
	static vector<unsigned char> serializeResponse(GetPersonalStatsResponse response);

	static vector<unsigned char> serializeResponse(CloseRoomResponse response);
	static vector<unsigned char> serializeResponse(StartGameResponse response);
	static vector<unsigned char> serializeResponse(GetRoomStateResponse response);
	static vector<unsigned char> serializeResponse(LeaveRoomResponse response);

	static vector<unsigned char> serializeResponse(GetGameResultsResponse response);
	static vector<unsigned char> serializeResponse(SubmitAnswerResponse response);
	static vector<unsigned char> serializeResponse(GetQuestionResponse response);
	static vector<unsigned char> serializeResponse(LeaveGameResponse response);

private:
	static string vectorToString(vector<string> msg);
};

