#include "RoomManager.h"

Room& RoomManager::createRoom(LoggedUser user, RoomData roomData)
{
    Room* newRoom = new Room(user, roomData);
    _rooms.insert(std::pair<int, Room*>(roomData.id, newRoom));
    return *newRoom;
}


void RoomManager::CleanRooms()
{
    for (auto it = _rooms.begin(); it != _rooms.end(); ++it)
    {
        if ((*it->second).getAllUsers().size() == 0)
        {
            _rooms.erase(it);
            break;
        }
    }
}

bool RoomManager::canCreate(LoggedUser user, RoomData roomData)
{
    for (auto it = _rooms.begin(); it != _rooms.end(); ++it)
    {
        if ((*it->second).getRoomData().name == roomData.name) return 0;
    }
    return 1;
}

Room& RoomManager::getRoom(string roomName)
{
    return *getRoomsMap()[getRoomId(roomName)];
}


vector<RoomData> RoomManager::getRooms()
{
    vector<RoomData> roomsData;

    for (auto room : _rooms)
    {
        roomsData.push_back((*room.second).getRoomData());
    }

    return roomsData;
}

map<unsigned, Room*>& RoomManager::getRoomsMap()
{
    return _rooms;
}

int RoomManager::getNewId()
{
    int id = 1;
    for (auto it = _rooms.begin(); it != _rooms.end(); ++it)
    {
        if (it->first == id) id++;
    }
    return id;
}

int RoomManager::getRoomId(string name)
{
    for (auto it = _rooms.begin(); it != _rooms.end(); ++it)
    {
        if ((*it->second).getRoomData().name == name) return it->first;
    }
}

void RoomManager::removePlayer(string player)
{
    for (auto& room : _rooms)
    {
        auto players = (*room.second).getAllUsers();
        for (auto it = players.begin(); it != players.end(); it++)
        {
            if (player.compare(*it) == 0)
            {
                (*room.second).removeUser(LoggedUser(player));
                break;
            }
        }
    }
}

