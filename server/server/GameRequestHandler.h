#pragma once
#include <map>
#include "Question.h"
#include "Responses.h"
#include "GameManager.h"
#include "LoggedUser.h"
#include "RequestResult.h"
#include "IRequestHandler.h"
#include "RequestHandlerFactory.h"

#define GET_GAME_RESULT_CODE 'O'
#define SUBMIT_ANSWER_CODE 'P'
#define GET_QUESTION_CODE 'Q'
#define LEAVE_GAME_CODE 'R'

using std::map;

class RequestHandlerFactory;

class GameRequestHandler : public IRequestHandler
{

public:
	GameRequestHandler(RequestHandlerFactory& handlerFacroty, GameManager& gameManager, LoggedUser user, Game& game);
	~GameRequestHandler();
	
	virtual bool isRequestRelevant(RequestInfo requestInfo) override;
	virtual RequestResult handleRequest(RequestInfo requestInfo) override;

private:
	RequestResult getQuestion(RequestInfo requestInfo);
	RequestResult submitAnswer(RequestInfo requestInfo);
	RequestResult getGameResults(RequestInfo requestInfo);
	RequestResult leaveGame(RequestInfo requestInfo);

	unsigned int getAnswerID(vector<string> answers, string answer);

	Game& _game;
	LoggedUser _user;
	GameManager&  _gameManager;
	RequestHandlerFactory&  _handlerFacroty;

	map<LoggedUser, Question> _usersQuestions;

};

