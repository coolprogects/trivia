#pragma once
#include "RequestResult.h"
#include "IRequestHandler.h"
#include "RequestHandlerFactory.h"

#define SIGN_UP_CODE 'C'
#define LOGIN_CODE 'B'

class RequestHandlerFactory;

class LoginRequestHandler : public IRequestHandler
{

public:
	LoginRequestHandler(LoginManager& loginManager, RequestHandlerFactory& handlerFactory);
	~LoginRequestHandler();

	virtual bool isRequestRelevant(RequestInfo requestInfo) override;
	virtual RequestResult handleRequest(RequestInfo requestInfo) override;

private:
	LoginManager& _loginManager;
	RequestHandlerFactory& _handlerFactory;

	RequestResult error(RequestInfo requestInfo); // OUR FUNCTION !!!
	RequestResult login(RequestInfo requestInfo);
	RequestResult signup(RequestInfo requestInfo);

};

