#include "JsonRequestPacketDeserializer.h"

LoginRequest JsonRequestPacketDeserializer::deserializeLoginRequest(vector<unsigned char> buffer)
{
	json bufferJson = json::parse(buffer);

	// building the struct.
	LoginRequest request
	{
		bufferJson["username"].get<string>(),
		bufferJson["password"].get<string>()
	};

	return request;
}

SignupRequest JsonRequestPacketDeserializer::deserializeSignupRequest(vector<unsigned char> buffer)
{
	json bufferJson = json::parse(buffer);

	// building the struct.
	SignupRequest request
	{
		bufferJson["username"].get<string>(),
		bufferJson["password"].get<string>(),
		bufferJson["email"].get<string>()
	};

	return request;
}

GetPlayersInRoomRequest JsonRequestPacketDeserializer::deserializeGetPlayersRequest(vector<unsigned char> buffer)
{
	json bufferJson = json::parse(buffer);

	// building the struct.
	GetPlayersInRoomRequest request
	{
		bufferJson["roomName"].get<string>()
	};

	return request;
}

JoinRoomRequest JsonRequestPacketDeserializer::deserializeJoinRoomRequest(vector<unsigned char> buffer)
{
	json bufferJson = json::parse(buffer);

	// building the struct.
	JoinRoomRequest request
	{
		//bufferJson["roomID"].get<unsigned int>()
		bufferJson["roomName"].get<string>()
	};

	return request;
}

CreateRoomRequest JsonRequestPacketDeserializer::deserializeCreateRoomRequest(vector<unsigned char> buffer)
{
	json bufferJson = json::parse(buffer);

	// building the struct.
	CreateRoomRequest request
	{
		bufferJson["roomName"].get<string>(),
		bufferJson["maxUsers"].get<unsigned int>(),
		bufferJson["questionCount"].get<unsigned int>(),
		bufferJson["answerTimeout"].get<unsigned int>()
	};

	return request;
}

SubmitAnswerRequest JsonRequestPacketDeserializer::deserializerSubmitAnswerRequest(vector<unsigned char> buffer)
{
	json bufferJson = json::parse(buffer);

	// building the struct.
	SubmitAnswerRequest request
	{
		bufferJson["answerId"].get<unsigned int>(),
		bufferJson["secondAmount"].get<unsigned int>(),
	};

	return request;
}
