#include <fstream>
#include "SqliteDatabase.h"
#include <nlohmann/json.hpp>

vector<string> SqliteDatabase::_values;

using std::cout;
using std::endl;
using std::stof;
using std::stoi;
using std::ifstream;
using std::to_string;
using nlohmann::json;

SqliteDatabase::SqliteDatabase()
{
    vector<string> sqlStatements;
    string dbFileName = "TDB.sqlite";
    int doesFileExist = _access(dbFileName.c_str(), 0);
    int res = sqlite3_open(dbFileName.c_str(), &_db);

    if (res != SQLITE_OK)
    {
        _db = nullptr;
        cout << "Failed to open DB" << endl;
    }

    else if (doesFileExist == -1)
    {
        // creating sql tables
        string userStatement = "CREATE TABLE User (username TEXT PRIMARY KEY NOT NULL, password TEXT NOT NULL, email TEXT NOT NULL);";
        string questionStatement = "CREATE TABLE Question (question TEXT PRIMARY KEY NOT NULL, correct_answer TEXT NOT NULL, answer_1 TEXT NOT NULL, answer_2 TEXT NOT NULL, answer_3 TEXT NOT NULL);";
        string statisticsStatement = "CREATE TABLE Statistics (username TEXT PRIMARY KEY NOT NULL, games_played INT NOT NULL, total_answers INT NOT NULL, correct_answers INT NOT NULL, answering_time FLOAT NOT NULL);";

        sqlStatements.push_back(userStatement);
        sqlStatements.push_back(questionStatement);
        sqlStatements.push_back(statisticsStatement);

        // excecuting sql statements
        for (auto sqlStatement : sqlStatements)
        {
            res = excecuteSql(sqlStatement, nullptr);

            if (res != SQLITE_OK)
            {
                cout << "An error has accured!";
            }
        }

        addQuestions();
    }
}

SqliteDatabase::~SqliteDatabase()
{
    sqlite3_close(_db);
    _db = nullptr;
}

bool SqliteDatabase::doesUserExist(string username)
{
    bool doesExist;

    string sqlStatement = "SELECT COUNT(1) FROM User WHERE username='" + username + "';";
    excecuteSql(sqlStatement, callback);

    doesExist = _values.back() != "0";
    _values.clear();

    return doesExist;
}

bool SqliteDatabase::doesPasswordMatch(string username, string password)
{
    bool doesMatch;

    string sqlStatement = "SELECT COUNT(1) FROM User WHERE username='" + username + "' AND password='" + password + "';";
    excecuteSql(sqlStatement, callback);

    doesMatch = _values.back() != "0";
    _values.clear();

    return doesMatch;
}

void SqliteDatabase::addNewUser(string username, string password, string email)
{
    string sqlStatement = "INSERT INTO User(username, password, email) VALUES('" + username + "', '" + password + "', '" + email + "');";
    excecuteSql(sqlStatement, nullptr);

    sqlStatement = "INSERT INTO Statistics(username, games_played, total_answers, correct_answers, answering_time) VALUES('" + username + "', 0, 0, 0, 0);";
    excecuteSql(sqlStatement, nullptr);
}

float SqliteDatabase::getPlayerAverageAnswerTime(string username)
{
    int questionsAnswerd;
    float totalAnsweringTime;

    string sqlStatement = "SELECT total_answers, answering_time FROM Statistics WHERE username='" + username + "';";
    excecuteSql(sqlStatement, callback);

    totalAnsweringTime = stof(_values.back());
    _values.pop_back();
    questionsAnswerd = stoi(_values.back());
    _values.pop_back();

    _values.clear();

    if (questionsAnswerd == 0) return 0;
    return totalAnsweringTime / questionsAnswerd;
}

int SqliteDatabase::getNumOfCorrectAnswers(string username)
{
    int numOfCorrectAnswers;

    string sqlStatement = "SELECT correct_answers FROM Statistics WHERE username='" + username + "';";
    excecuteSql(sqlStatement, callback);

    numOfCorrectAnswers = stoi(_values.back());
    _values.clear();

    return numOfCorrectAnswers;
}

int SqliteDatabase::getNumOfTotalAnswers(string username)
{
    int numOfTotalAnswers;

    string sqlStatement = "SELECT total_answers FROM Statistics WHERE username='" + username + "';";
    excecuteSql(sqlStatement, callback);

    numOfTotalAnswers = stoi(_values.back());
    _values.clear();

    return numOfTotalAnswers;
}

int SqliteDatabase::getNumOfPlayerGames(string username)
{
    int numOfPlayerGames;

    string sqlStatement = "SELECT games_played FROM Statistics WHERE username='" + username + "';";
    excecuteSql(sqlStatement, callback);

    numOfPlayerGames = stoi(_values.back());
    _values.clear();

    return numOfPlayerGames;
}

vector<Question> SqliteDatabase::getQuestions(int amount)
{
    int counter = 0;
    vector<Question> questions;
    vector<string> questionValues;
    vector<string> possibleAnswers;
    string sqlStatement = "SELECT question, correct_answer, answer_1, answer_2, answer_3 FROM Question LIMIT " + to_string(amount) + ";";
    excecuteSql(sqlStatement, callback);

    for (auto value : _values)
    {
        counter++;
        questionValues.push_back(value);

        if (counter % 5 == 0)
        {
            possibleAnswers.push_back(questionValues[1]);
            possibleAnswers.push_back(questionValues[2]);
            possibleAnswers.push_back(questionValues[3]);
            possibleAnswers.push_back(questionValues[4]);
            questions.push_back(Question(questionValues[0], questionValues[1], possibleAnswers));
            questionValues.clear();
            possibleAnswers.clear();
        }
    }

    _values.clear();

    return questions;
}

vector<string> SqliteDatabase::getAllUsers()
{
    string sqlStatement = "SELECT username FROM User;";
    excecuteSql(sqlStatement, callback);

    vector<string> users(_values);

    _values.clear();

    return users;
}

void SqliteDatabase::updateGameResult(string username, int correctAnswerCount, int questionsAmount, int AnswerTimeAmount)
{
    
    int games_played, total_answers, correct_answers;
    float answering_time;
    string sqlStatement = "SELECT games_played, total_answers, correct_answers, answering_time FROM Statistics WHERE username = '" + username + "' ;";
    excecuteSql(sqlStatement, callback);

    vector<string> lastUpdate(_values);
    _values.clear();
    
    games_played = std::stoi(lastUpdate[0]);
    total_answers = std::stoi(lastUpdate[1]);
    correct_answers = std::stoi(lastUpdate[2]);
    answering_time = std::stof(lastUpdate[3]);

    answering_time = (answering_time * total_answers + AnswerTimeAmount) / (total_answers + questionsAmount);
    games_played++;
    correct_answers += correctAnswerCount;
    total_answers += questionsAmount;


    sqlStatement = "UPDATE Statistics SET games_played = "+ to_string(games_played) + ", total_answers = " + to_string(total_answers) + ", correct_answers = " + to_string(correct_answers) + ", answering_time = " + to_string(answering_time) + " WHERE username = '" + username +"';";
    excecuteSql(sqlStatement, callback);

    _values.clear();

    sqlStatement = "SELECT games_played, total_answers, correct_answers, answering_time FROM Statistics WHERE username = '" + username + "' ;";
    excecuteSql(sqlStatement, callback);
    _values.clear();
}

void SqliteDatabase::addQuestions()
{
    json questionsJson;
    string sqlStatement;
    vector<string> incorrect_answers;
    ifstream questions_file("questions.json");

    // getting the json data from the json file
    questions_file >> questionsJson;

    // Parsing the json data from the json
    for (auto questionJson : questionsJson["results"])
    {
        for (auto incorrect_answer : questionJson["incorrect_answers"])
        {
            incorrect_answers.push_back(incorrect_answer.get<string>());
        }

        sqlStatement = "INSERT INTO Question VALUES('" + questionJson["question"].get<string>() + "', '" + incorrect_answers[0] + "', '" + incorrect_answers[1] + "', '" + incorrect_answers[2] + "', '" + questionJson["correct_answer"].get<string>() + "');";
        excecuteSql(sqlStatement, nullptr);
        incorrect_answers.clear();
    }
}

int SqliteDatabase::excecuteSql(string sqlStatement, int(*callback)(void*, int, char**, char**))
{
    char* errMessage = nullptr;
    int res = sqlite3_exec(_db, sqlStatement.c_str(), callback, nullptr, &errMessage);
    return res;
}

int SqliteDatabase::callback(void* NotUsed, int argc, char** argv, char** azColName)
{
    for (int i = 0; i < argc; i++)
    {
        _values.push_back(string(argv[i]));
    }

    return 0;
}