#include "RoomMemberRequestHandler.h"
#include "JsonResponsePacketSerializer.h"
#include "JsonRequestPacketDeserializer.h"


RoomMemberRequestHandler::RoomMemberRequestHandler(RequestHandlerFactory& handlerFactory, RoomManager& roomManager, Room& room, LoggedUser user)
 : _handlerFactory(handlerFactory), _roomManager(roomManager), _room(room)
{
    _user = user;
}

RoomMemberRequestHandler::~RoomMemberRequestHandler()
{
}

bool RoomMemberRequestHandler::isRequestRelevant(RequestInfo requestInfo)
{
	return requestInfo.id == LEAVE_ROOM_CODE || requestInfo.id == ROOM_STATE_CODE;
}

RequestResult RoomMemberRequestHandler::handleRequest(RequestInfo requestInfo)
{
	RequestResult result;
	
    switch (requestInfo.id)
    {

    case LEAVE_ROOM_CODE:
    {
        result = leaveRoom(requestInfo);
        break;
    }

    case ROOM_STATE_CODE:
    {
        result = getRoomState(requestInfo);
        break;
    }
    }
	return result;
}

RequestResult RoomMemberRequestHandler::getRoomState(RequestInfo requestInfo)
{
    
    GetRoomStateResponse request
    {
            1,
        _room.getRoomData().isActive,
        _room.getAllUsers(),
        _room.getRoomData().numOfQuestionsInGame,
        _room.getRoomData().timePerQuestion
    };
 
    RequestResult result;
    vector<unsigned char> buffer = JsonResponsePacketSerializer::serializeResponse(request);
    result.buffer = buffer;
    
    if (request.hasGameBegun == 1)
    {
        _room.removeUser(_user);
        _roomManager.CleanRooms();

        Game& game = _handlerFactory.getGameManager().getGame(_user);
        result.newHandler = _handlerFactory.createGameRequestHandler(_user, game);  
    }
    else if (request.hasGameBegun == 0) result.newHandler = _handlerFactory.createRoomMemberRequestHandler(_room, _user);
    else
    {
        result.newHandler = _handlerFactory.createMenuRequestHandler(_user);
        _room.removeUser(_user);
        _roomManager.CleanRooms();
    }


    return result;
}

RequestResult RoomMemberRequestHandler::leaveRoom(RequestInfo requestInfo)
{
    LeaveRoomResponse request
    {
        1
    };

    _room.removeUser(_user);
    _roomManager.CleanRooms();

    vector<unsigned char> buffer = JsonResponsePacketSerializer::serializeResponse(request);
    MenuRequestHandler* handler = _handlerFactory.createMenuRequestHandler(_user);

    RequestResult result
    {
        buffer,
        handler
    };

    return result;
}