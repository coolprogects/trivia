#include "StatisticsManager.h"

StatisticsManager::StatisticsManager()
{
	_database = new SqliteDatabase();
}

StatisticsManager::~StatisticsManager()
{
}

vector<string> StatisticsManager::getHighScore()
{
	vector<string> highestScores;
	map<string, float> userToGrade;
	map<string, int> userXp;
	float averageAnswerTime, score;
	int numOfPlayerGames, numOfTotalAnswers, numOfCorrectAnswers;

	// Calculating the formula
	for (auto username : _database->getAllUsers())
	{
		numOfTotalAnswers = _database->getNumOfTotalAnswers(username);
		numOfCorrectAnswers = _database->getNumOfCorrectAnswers(username);
		averageAnswerTime = _database->getPlayerAverageAnswerTime(username);
		if (averageAnswerTime == 0) score = 0;
		else score = ((float)numOfCorrectAnswers / (float)numOfTotalAnswers);// / averageAnswerTime;

		userXp[username] = numOfCorrectAnswers * 10;

		userToGrade.insert(pair<string, float>(username, score));
	}

	// Sorting the map by value (highest score)
	vector<pair<string, float>> vec;

	copy(userToGrade.begin(), userToGrade.end(), back_inserter<vector<pair<string, float>>>(vec));

	sort(vec.begin(), vec.end(), [](const pair<string, float>& l, const pair<string, float>& r)
		{
			if (l.second != r.second)
			{
				return l.second > r.second;
			}
		
			return l.first < r.first;
		});

	// Adding the top 5 scoring players
	for (int i = 0; i < NUM_OF_HIGH_SCORES; i++)
	{
		auto it = vec.begin();
		std::advance(it, i);
		highestScores.push_back(it->first);
		highestScores.push_back(std::to_string(userXp[it->first]));
		highestScores.push_back(std::to_string(it->second));
	}

	return highestScores;
}

vector<string> StatisticsManager::getUserStatistics(string username)
{
	vector<string> statistics;

	int numOfPlayerGames = _database->getNumOfPlayerGames(username);
	int numOfTotalAnswers = _database->getNumOfTotalAnswers(username);
	int numOfCorrectAnswers = _database->getNumOfCorrectAnswers(username);
	float averageAnswerTime = _database->getPlayerAverageAnswerTime(username);

	statistics.push_back(to_string(numOfPlayerGames));
	statistics.push_back(to_string(numOfTotalAnswers));
	statistics.push_back(to_string(numOfCorrectAnswers));
	statistics.push_back(to_string(averageAnswerTime));


	return statistics;
}
