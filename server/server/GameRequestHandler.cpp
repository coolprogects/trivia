#include "Requests.h"
#include "GameRequestHandler.h"
#include "JsonResponsePacketSerializer.h"
#include "JsonRequestPacketDeserializer.h"

GameRequestHandler::GameRequestHandler(RequestHandlerFactory& handlerFacroty, GameManager& gameManager, LoggedUser user, Game& game)
    : _handlerFacroty(handlerFacroty), _gameManager(gameManager), _user(user), _game(game)
{   

}

GameRequestHandler::~GameRequestHandler()
{
}

bool GameRequestHandler::isRequestRelevant(RequestInfo requestInfo)
{
    return requestInfo.id >= GET_GAME_RESULT_CODE && requestInfo.id <= LEAVE_GAME_CODE;
}

RequestResult GameRequestHandler::handleRequest(RequestInfo requestInfo)
{
    RequestResult result;

    switch (requestInfo.id)
    {
    case GET_GAME_RESULT_CODE:
        result = getGameResults(requestInfo);
        break;
    case SUBMIT_ANSWER_CODE:
        result = submitAnswer(requestInfo);
        break;
    case GET_QUESTION_CODE:
        result = getQuestion(requestInfo);
        break;
    case LEAVE_GAME_CODE:
        result = leaveGame(requestInfo);
        break;
    }

    return result;
}

RequestResult GameRequestHandler::getQuestion(RequestInfo requestInfo)
{
    map<unsigned int, string> answers;

    int success = _game.isGameOver(_user);
    
    if(success == 1)
    {
        _usersQuestions[LoggedUser(_user)] = _game.getQuestionForUser(_user);

        for (int i = 1; i <= _usersQuestions[LoggedUser(_user)].getPossibleAnswers().size(); i++)
        {
            answers[i] = _usersQuestions[LoggedUser(_user)].getPossibleAnswers()[i - 1];
        }
    }

    GetQuestionResponse response
    {
        success,
        _usersQuestions[LoggedUser(_user)].getQuestion(),
        answers
    };

    vector<unsigned char> buffer = JsonResponsePacketSerializer::serializeResponse(response);
    GameRequestHandler* handler = _handlerFacroty.createGameRequestHandler(_user, _game);

    RequestResult result
    {
        buffer,
        handler,
    };

    return result;
}

RequestResult GameRequestHandler::submitAnswer(RequestInfo requestInfo)
{
    SubmitAnswerRequest request = JsonRequestPacketDeserializer::deserializerSubmitAnswerRequest(requestInfo.buffer);
    _game.submitAnswer(_user, request.answerId, request.secondAmount);
    

    SubmitAnswerResponse response
    {
        1,
        getAnswerID(_game.getPlayers()[_user].currentQuestion.getPossibleAnswers(), _game.getPlayers()[_user].currentQuestion.getCorrectAnswer())
    };

    vector<unsigned char> buffer = JsonResponsePacketSerializer::serializeResponse(response);
    GameRequestHandler* handler = _handlerFacroty.createGameRequestHandler(_user, _game);

    RequestResult result
    {
        buffer,
        handler
    };

    return result;
}

RequestResult GameRequestHandler::getGameResults(RequestInfo requestInfo)
{
    vector<PlayerResults> results;
                                    
    for (auto player : _game.getPlayers())
    {
        PlayerResults playerResult
        {
            player.first.getUsername(),
            player.second.correctAnswerCount,
            player.second.wrongAnswerCount,
            player.second.averangeAnswerTime / (float)_game.getQuestions().size()
        };

        results.push_back(playerResult);
    }

    _gameManager.gameResult(_game, _user);
    _game.leaveGame();
    if(_game.gameEnded())_gameManager.deleteGame(_game);

    GetGameResultsResponse response
    {
        1,
        results
    };

    vector<unsigned char> buffer = JsonResponsePacketSerializer::serializeResponse(response);
    MenuRequestHandler* handler = _handlerFacroty.createMenuRequestHandler(_user);

    RequestResult result
    {
        buffer,
        handler,
    };

    return result;
}

RequestResult GameRequestHandler::leaveGame(RequestInfo requestInfo)
{
    _game.removePlayer(_user);

    LeaveGameResponse response
    {
        1
    };

    vector<unsigned char> buffer = JsonResponsePacketSerializer::serializeResponse(response);
    MenuRequestHandler* handler = _handlerFacroty.createMenuRequestHandler(_user);

    RequestResult result
    {
        buffer,
        handler,
    };

    return result;
}

unsigned int GameRequestHandler::getAnswerID(vector<string> answers, string answer)
{
    for (int i = 0; i < answers.size(); i++)
    {
        if (answers[i] == answer)
        {
            return i + 1;
        }
    }
}
