#include "Game.h"
#include "SqliteDatabase.h"
#include <algorithm>

using std::find;
using std::distance;

Game::Game(Room room)
{
    SqliteDatabase db = SqliteDatabase();
    _questions = db.getQuestions(room.getRoomData().numOfQuestionsInGame);
    playersAmount = room.getAllUsers().size();

    for (auto user : room.getAllUsers())
    {
        GameData gameData
        {
            //getQuestionForUser(LoggedUser(user)),
            Question(),
            0,
            0,
            0
        };

        _players[user] = gameData;
    }
}

Question Game::getQuestionForUser(LoggedUser user)
{
    vector<Question>::iterator itr = find(_questions.begin(), _questions.end(), _players[user].currentQuestion);
    _players[user].currentQuestion = _questions[distance(_questions.begin(), itr) - 1];
    return _questions[distance(_questions.begin(), itr) - 1];
}

void Game::submitAnswer(LoggedUser user, unsigned int answerID, unsigned int seconds)
{
    if (_players[user].currentQuestion.getPossibleAnswers()[answerID - 1] == _players[user].currentQuestion.getCorrectAnswer())
    {
        _players[user].correctAnswerCount++;
    }
    else
    {
        _players[user].wrongAnswerCount++;
    }
    _players[user].averangeAnswerTime += seconds;
}

void Game::removePlayer(LoggedUser user)
{
    _players.erase(user);
}

bool Game::isGameOver(LoggedUser user)
{
    vector<Question>::iterator itr = find(_questions.begin(), _questions.end(), _players[user].currentQuestion);
    return !(itr == _questions.begin());
}

map<LoggedUser, GameData> Game::getPlayers()
{
    return _players;
}

vector<Question> Game::getQuestions()
{
    return this->_questions;
}

bool Game::operator==(const Game& other) const
{
    return other._players == this->_players;
}

bool Game::operator<(const Game& other) const
{
    return _questions.size() < other._questions.size();
}

bool Game::operator>(const Game& other) const
{
    return _questions.size() > other._questions.size();
}

bool Game::IsPlayerExist(LoggedUser user)
{
    vector<LoggedUser> players;
    
    for (auto player : _players) players.push_back(player.first);
    
    return find(players.begin(), players.end(), user) != players.end();
}

void Game::leaveGame()
{
    playersAmount--;
}

bool Game::gameEnded()
{
    return playersAmount == 0;
}
