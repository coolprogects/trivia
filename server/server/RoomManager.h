#pragma once
#include "Room.h"
#include <map>

using std::map;

class RoomManager
{

public:
	Room& createRoom(LoggedUser user, RoomData roomData);
	//unsigned int getRoomState(unsigned int ID);
	vector<RoomData> getRooms();
	
	// OUR FUNCTION !!!
	map<unsigned, Room*>& getRoomsMap();
	int getNewId();
	int getRoomId(string name);
	void removePlayer(string player);
	void CleanRooms();
	bool canCreate(LoggedUser user, RoomData roomData);
	Room& getRoom(string roomName);

private:
	map<unsigned int, Room*> _rooms;

};

