#include "JsonResponsePacketSerializer.h"

using std::to_string;

string statusString = "status: \"";
string messageString = "message: \"";

vector<unsigned char> JsonResponsePacketSerializer::serializeResponse(ErrorResponse response)
{
    int sizeInt;
    json message;
    string size, messageStr;
    vector<unsigned char> buffer;

    // Building the json
    message["message"] = response.message;

    // Protocol code
    buffer.push_back('A');

    // Message's size
    sizeInt = sizeof(message);
    size = to_string(sizeInt);
    size.insert(0, 4 - size.length(), '0'); // Making sure that the size is 4 bytes
    buffer.insert(buffer.end(), size.begin(), size.end());

    // Message
    messageStr = message.dump();
    buffer.insert(buffer.end(), messageStr.begin(), messageStr.end());

    return buffer;
}

vector<unsigned char> JsonResponsePacketSerializer::serializeResponse(LoginResponse response)
{
    int sizeInt;
    json message;
    string size, messageStr;
    vector<unsigned char> buffer;

    // Building the json
    message["status"] = response.status;

    // Protocol code
    buffer.push_back('B');

    // Message's size
    sizeInt = sizeof(message);
    size = to_string(sizeInt);
    size.insert(0, 4 - size.length(), '0'); // Making sure that the size is 4 bytes
    buffer.insert(buffer.end(), size.begin(), size.end());

    // Message
    messageStr = message.dump();
    buffer.insert(buffer.end(), messageStr.begin(), messageStr.end());

    return buffer;
}

vector<unsigned char> JsonResponsePacketSerializer::serializeResponse(SignupResponse response)
{
    int sizeInt;
    json message;
    string size, messageStr;
    vector<unsigned char> buffer;

    // Building the json
    message["status"] = response.status;

    // Protocol code
    buffer.push_back('C');

    // Message's size
    sizeInt = sizeof(message);
    size = to_string(sizeInt);
    size.insert(0, 4 - size.length(), '0'); // Making sure that the size is 4 bytes
    buffer.insert(buffer.end(), size.begin(), size.end());

    // Message
    messageStr = message.dump();
    buffer.insert(buffer.end(), messageStr.begin(), messageStr.end());

    return buffer;
}

vector<unsigned char> JsonResponsePacketSerializer::serializeResponse(LogoutResponse response)
{
    int sizeInt;
    json message;
    string size, messageStr;
    vector<unsigned char> buffer;

    // Building the json
    message["status"] = response.status;

    // Protocol code
    buffer.push_back('D');

    // Message's size
    sizeInt = sizeof(message);
    size = to_string(sizeInt);
    size.insert(0, 4 - size.length(), '0'); // Making sure that the size is 4 bytes
    buffer.insert(buffer.end(), size.begin(), size.end());

    // Message
    messageStr = message.dump();
    buffer.insert(buffer.end(), messageStr.begin(), messageStr.end());

    return buffer;
}

vector<unsigned char> JsonResponsePacketSerializer::serializeResponse(GetRoomsResponse response)
{
    int sizeInt;
    json message;
    string size, messageStr;
    vector<string> roomsNames;
    vector<unsigned char> buffer;
    int success = 0;
    // Building the json

    for (auto roomData : response.rooms)
    {
        roomsNames.push_back(roomData.name);
    }

    if (roomsNames.size() > 0) success = 1;

    message["status"] = success;
    message["rooms"] = roomsNames;//vectorToString(roomsNames);

    // Protocol code
    buffer.push_back('E');

    // Message's size
    sizeInt = sizeof(message);
    size = to_string(sizeInt);
    size.insert(0, 4 - size.length(), '0'); // Making sure that the size is 4 bytes
    buffer.insert(buffer.end(), size.begin(), size.end());

    // Message
    messageStr = message.dump();
    buffer.insert(buffer.end(), messageStr.begin(), messageStr.end());

    return buffer;
}

vector<unsigned char> JsonResponsePacketSerializer::serializeResponse(GetPlayersInRoomResponse response)
{
    int sizeInt;
    json message;
    string size, messageStr;
    vector<unsigned char> buffer;

    // Building the json
    message["players"] = response.players;

    // Protocol code
    buffer.push_back('F');

    // Message's size
    sizeInt = sizeof(message);
    size = to_string(sizeInt);
    size.insert(0, 4 - size.length(), '0'); // Making sure that the size is 4 bytes
    buffer.insert(buffer.end(), size.begin(), size.end());

    // Message
    messageStr = message.dump();
    buffer.insert(buffer.end(), messageStr.begin(), messageStr.end());

    return buffer;
}

vector<unsigned char> JsonResponsePacketSerializer::serializeResponse(JoinRoomResponse response)
{
    int sizeInt;
    json message;
    string size, messageStr;
    vector<unsigned char> buffer;

    // Building the json
    message["status"] = response.status;

    // Protocol code
    buffer.push_back('G');

    // Message's size
    sizeInt = sizeof(message);
    size = to_string(sizeInt);
    size.insert(0, 4 - size.length(), '0'); // Making sure that the size is 4 bytes
    buffer.insert(buffer.end(), size.begin(), size.end());

    // Message
    messageStr = message.dump();
    buffer.insert(buffer.end(), messageStr.begin(), messageStr.end());

    return buffer;
}

vector<unsigned char> JsonResponsePacketSerializer::serializeResponse(CreateRoomResponse response)
{
    int sizeInt;
    json message;
    string size, messageStr;
    vector<unsigned char> buffer;

    // Building the json
    message["status"] = response.status;

    // Protocol code
    buffer.push_back('H');

    // Message's size
    sizeInt = sizeof(message);
    size = to_string(sizeInt);
    size.insert(0, 4 - size.length(), '0'); // Making sure that the size is 4 bytes
    buffer.insert(buffer.end(), size.begin(), size.end());

    // Message
    messageStr = message.dump();
    buffer.insert(buffer.end(), messageStr.begin(), messageStr.end());

    return buffer;
}

vector<unsigned char> JsonResponsePacketSerializer::serializeResponse(GetHighScoreResponse response)
{
    int sizeInt;
    json message;
    string size, messageStr;
    string Stats;
    vector<unsigned char> buffer;

    // Building the json
    message["status"] = response.status;
    message["leaderboard"] = response.leaderboard;


    // Protocol code
    buffer.push_back('J');

    // Message's size
    sizeInt = sizeof(message);
    size = to_string(sizeInt);
    size.insert(0, 4 - size.length(), '0'); // Making sure that the size is 4 bytes
    buffer.insert(buffer.end(), size.begin(), size.end());

    // Message
    messageStr = message.dump();
    buffer.insert(buffer.end(), messageStr.begin(), messageStr.end());

    return buffer;
}

vector<unsigned char> JsonResponsePacketSerializer::serializeResponse(GetPersonalStatsResponse response)
{
    int sizeInt;
    json message;
    string size, messageStr;
    string Stats;
    vector<unsigned char> buffer;

    // Building the json
    message["status"] = response.status;
    message["statistics"] = response.statistics;


    // Protocol code
    buffer.push_back('I');

    // Message's size
    sizeInt = sizeof(message);
    size = to_string(sizeInt);
    size.insert(0, 4 - size.length(), '0'); // Making sure that the size is 4 bytes
    buffer.insert(buffer.end(), size.begin(), size.end());

    // Message
    messageStr = message.dump();
    buffer.insert(buffer.end(), messageStr.begin(), messageStr.end());

    return buffer;
}

vector<unsigned char> JsonResponsePacketSerializer::serializeResponse(CloseRoomResponse response)
{
    int sizeInt;
    json message;
    string size, messageStr;
    vector<unsigned char> buffer;

    // Building the json
    message["status"] = response.status;

    // Protocol code
    buffer.push_back('K');

    // Message's size
    sizeInt = sizeof(message);
    size = to_string(sizeInt);
    size.insert(0, 4 - size.length(), '0'); // Making sure that the size is 4 bytes
    buffer.insert(buffer.end(), size.begin(), size.end());

    // Message
    messageStr = message.dump();
    buffer.insert(buffer.end(), messageStr.begin(), messageStr.end());

    return buffer;
}

vector<unsigned char> JsonResponsePacketSerializer::serializeResponse(StartGameResponse response)
{
    int sizeInt;
    json message;
    string size, messageStr;
    vector<unsigned char> buffer;

    // Building the json
    message["status"] = response.status;

    // Protocol code
    buffer.push_back('M');

    // Message's size
    sizeInt = sizeof(message);
    size = to_string(sizeInt);
    size.insert(0, 4 - size.length(), '0'); // Making sure that the size is 4 bytes
    buffer.insert(buffer.end(), size.begin(), size.end());

    // Message
    messageStr = message.dump();
    buffer.insert(buffer.end(), messageStr.begin(), messageStr.end());

    return buffer;
}

vector<unsigned char> JsonResponsePacketSerializer::serializeResponse(GetRoomStateResponse response)
{
    int sizeInt;
    json message;
    string size, messageStr;
    vector<unsigned char> buffer;

    // Building the json
    message["status"] = response.status;
    message["hasGameBegun"] = response.hasGameBegun;
    message["players"] = response.players;
    message["questionCount"] = response.questionCount;
    message["answerTimeout"] = response.answerTimeout;

    // Protocol code
    buffer.push_back('L');

    // Message's size
    sizeInt = sizeof(message);
    size = to_string(sizeInt);
    size.insert(0, 4 - size.length(), '0'); // Making sure that the size is 4 bytes
    buffer.insert(buffer.end(), size.begin(), size.end());

    // Message
    messageStr = message.dump();
    buffer.insert(buffer.end(), messageStr.begin(), messageStr.end());

    return buffer;
}

vector<unsigned char> JsonResponsePacketSerializer::serializeResponse(LeaveRoomResponse response)
{
    int sizeInt;
    json message;
    string size, messageStr;
    vector<unsigned char> buffer;

    // Building the json
    message["status"] = response.status;

    // Protocol code
    buffer.push_back('N');

    // Message's size
    sizeInt = sizeof(message);
    size = to_string(sizeInt);
    size.insert(0, 4 - size.length(), '0'); // Making sure that the size is 4 bytes
    buffer.insert(buffer.end(), size.begin(), size.end());

    // Message
    messageStr = message.dump();
    buffer.insert(buffer.end(), messageStr.begin(), messageStr.end());

    return buffer;
}

vector<unsigned char> JsonResponsePacketSerializer::serializeResponse(GetGameResultsResponse response)
{
    int sizeInt;
    json message;
    string size, messageStr;
    vector<unsigned char> buffer;
    map<string, vector<float >> result;

    for (auto it = response.results.begin(); it != response.results.end(); it++)
    {
        result[(*it).username] = { (*it).correctAnswerCount, (*it).wrongAnswerCount, (*it).averageAnswerTime};
    }
    
    // Building the json
    message["status"] = response.status;
    message["results"] = result;

    
    // Protocol code
    buffer.push_back('O');


    // Message's size
    sizeInt = sizeof(message);
    size = to_string(sizeInt);
    size.insert(0, 4 - size.length(), '0'); // Making sure that the size is 4 bytes
    buffer.insert(buffer.end(), size.begin(), size.end());

    
    // Message
    messageStr = message.dump();
    buffer.insert(buffer.end(), messageStr.begin(), messageStr.end());

    return buffer;
}

vector<unsigned char> JsonResponsePacketSerializer::serializeResponse(SubmitAnswerResponse response)
{
    int sizeInt;
    json message;
    string size, messageStr;
    vector<unsigned char> buffer;

    // Building the json
    message["status"] = response.status;
    message["correctAnswerId"] = response.correctAnswerId;
    
    // Protocol code
    buffer.push_back('P');


    // Message's size
    sizeInt = sizeof(message);
    size = to_string(sizeInt);
    size.insert(0, 4 - size.length(), '0'); // Making sure that the size is 4 bytes
    buffer.insert(buffer.end(), size.begin(), size.end());
    
    // Message
    messageStr = message.dump();
    buffer.insert(buffer.end(), messageStr.begin(), messageStr.end());

    return buffer;
}

vector<unsigned char> JsonResponsePacketSerializer::serializeResponse(GetQuestionResponse response)
{
    int sizeInt;
    json message;
    string size, messageStr;
    vector<unsigned char> buffer;

    // Building the json
    message["status"] = response.status;
    message["question"] = response.question;
    message["answers"] = response.answers;
    
    // Protocol code
    buffer.push_back('Q');


    // Message's size
    sizeInt = sizeof(message);
    size = to_string(sizeInt);
    size.insert(0, 4 - size.length(), '0'); // Making sure that the size is 4 bytes
    buffer.insert(buffer.end(), size.begin(), size.end());
    
    // Message
    messageStr = message.dump();
    buffer.insert(buffer.end(), messageStr.begin(), messageStr.end());

    return buffer;
}

vector<unsigned char> JsonResponsePacketSerializer::serializeResponse(LeaveGameResponse response)
{
    int sizeInt;
    json message;
    string size, messageStr;
    vector<unsigned char> buffer;

    // Building the json
    message["status"] = response.status;

    // Protocol code
    buffer.push_back('R');


    // Message's size
    sizeInt = sizeof(message);
    size = to_string(sizeInt);
    size.insert(0, 4 - size.length(), '0'); // Making sure that the size is 4 bytes
    buffer.insert(buffer.end(), size.begin(), size.end());
    
    // Message
    messageStr = message.dump();
    buffer.insert(buffer.end(), messageStr.begin(), messageStr.end());

    return buffer;
}

string JsonResponsePacketSerializer::vectorToString(vector<string> msg)
{
    string result;

    for (auto it = msg.begin(); it != msg.end(); it++)
    {
        result += *it;

        if (it != msg.end() - 1)
        {
            result += ", ";
        }
    }

    return result;
}
