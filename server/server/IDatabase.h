#pragma once
#include <string>
#include <vector>
#include "Question.h"

using std::string;
using std::vector;

class IDatabase
{

public:
	virtual bool doesUserExist(string username) = 0;
	virtual bool doesPasswordMatch(string username, string password) = 0;
	virtual void addNewUser(string username, string password, string email) = 0;

	virtual float getPlayerAverageAnswerTime(string username) = 0;
	virtual int getNumOfCorrectAnswers(string username) = 0;
	virtual int getNumOfTotalAnswers(string username) = 0;
	virtual int getNumOfPlayerGames(string username) = 0;

	virtual vector<Question> getQuestions(int amount) = 0;

	// our function
	virtual vector<string> getAllUsers() = 0;
	virtual void updateGameResult(string username, int correctAnswerCount, int questionsAmount, int AnswerTimeAmount) = 0;

};