#pragma warning(disable : 4996)
#include "Communicator.h"
#include <exception>
#include <numeric>
#include <string>
#include <ctime>
#include<algorithm>
#include <mutex>  

#include "RequestInfo.h"

#include <iostream>
#include <cstdlib>
//#include <unistd.h>
using namespace std;
#include <iostream>
#include <cstdlib>
//#include <unistd.h>


map<string, SOCKET> Communicator::_nameToSocket;

using std::stoi;
using std::time;
using std::ctime;
using std::pair;
using std::time_t;
using std::thread;
using std::string;
using std::exception;
using std::mutex;

mutex mtx_nameToSocket;
mutex mtx_clients;
mutex mtx_roomManager;
mutex mtx_loginManager;
mutex mtx_statisticsManager;
mutex mtx_GameManager;

//int number_of_create_room = 0;
//int number_of_statistic = 0;
//int number_of_join_room = 0;

//mutex mtx_count_createRoom;
//mutex mtx_count_joinRoom;
//mutex mtx_count_statistic;

static const unsigned short PORT = 55555;
static const unsigned int IFACE = 0;

Communicator::Communicator(RequestHandlerFactory& handlerFactory)
	:_handlerFactory(handlerFactory)
{
	//thread tr(&Communicator::display_count, this);
	
	_serverSocket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);

	this->startHandleRequests();
	if (_serverSocket == INVALID_SOCKET)
		throw exception(__FUNCTION__ " - socket");

}

Communicator::~Communicator()
{
	cout << __FUNCTION__ " closing accepting socket" << endl;

	try
	{
		closesocket(_serverSocket);
	}
	catch (...) {}
}

void Communicator::startHandleRequests()
{
	bindAndListen();

	while (true)
	{
		cout << "accepting client..." << endl;
		acceptClient();
	}
}


void Communicator::acceptClient()
{
	SOCKET clientSocket = accept(_serverSocket, NULL, NULL);
	if (clientSocket == INVALID_SOCKET)
		throw exception(__FUNCTION__);

	cout << "Client accepted !" << endl;

	// add client to clients map
	_clients.insert(pair<SOCKET, IRequestHandler*>(clientSocket, _handlerFactory.createLoginRequestHandler()));

	// create new thread for client	and detach from it
	thread tr(&Communicator::handleNewClient, this, clientSocket);
	tr.detach();
}

void Communicator::bindAndListen()
{
	struct sockaddr_in sa = { 0 };
	sa.sin_port = htons(PORT);
	sa.sin_family = AF_INET;
	sa.sin_addr.s_addr = IFACE;

	if (bind(_serverSocket, (struct sockaddr*)&sa, sizeof(sa)) == SOCKET_ERROR)
		throw exception(__FUNCTION__ " - bind");
	cout << "binded" << endl;

	if (listen(_serverSocket, SOMAXCONN) == SOCKET_ERROR)
		throw exception(__FUNCTION__ " - listen");
	cout << "listening..." << endl;
}

void Communicator::handleNewClient(const SOCKET clientSocket)
{
	try
	{
		while (true)
		{
			// Reciving login or sign up client option
			char returned_msg_array[1001] = {0};
			returned_msg_array[1000] = 0;
			recv(clientSocket, returned_msg_array, 1000, 0);

			char type = returned_msg_array[0]; // message's code
			string returned_msg = returned_msg_array;

			int len = stoi(returned_msg.substr(1, 4)); // message's length
			string data = returned_msg.substr(5, len); // message's data
			returned_msg = returned_msg.substr(0, len + 5);

			// current time
			time_t curr_time;
			curr_time = time(NULL);

			std::vector<unsigned char> buffer(data.begin(), data.end());

			RequestInfo requestInfo
			{
				type,
				ctime(&curr_time),
				buffer,
			};

			RequestResult requestResult;

			if (_clients[clientSocket]->isRequestRelevant(requestInfo))
			{
				requestResult = execution(clientSocket, requestInfo);
			}
			else
			{
				requestResult.newHandler = _clients[clientSocket];
				string notRelevantMessage = "A0016{\"message\":\"ERROR\"}";
				vector<unsigned char> notRelevantBuffer(notRelevantMessage.begin(), notRelevantMessage.end());
				requestResult.buffer = notRelevantBuffer;
			}
			_clients[clientSocket] = requestResult.newHandler;


			string response(requestResult.buffer.begin(), requestResult.buffer.end());

			send(clientSocket, response.c_str(), response.size(), 0);
			
			cout << "CLIENT: " << data << endl;
			cout << "SERVER: " << response << endl;

			addClient(returned_msg, response, clientSocket);
		}
	}

	catch (const exception& e)
	{
		cout << "Exception was catch in function clientHandler. socket=" << clientSocket << ", what=" << e.what() << endl;
		deleteUser(clientSocket);
	}

	closesocket(clientSocket);
}

void Communicator::addClient(string message, string response, SOCKET clientSocket)
{
	if (response[0] != 'B') return;
	int start = message.find(":") + 2;
	string userName = message.substr(start, message.find(",") - 1 - start); //  18,  22 - 1 - 18
	_nameToSocket[userName] = clientSocket;
}

void Communicator::deleteUser(SOCKET client_socket)
{
	std::lock_guard<mutex> lck1{ mtx_roomManager };
	std::lock_guard<mutex> lck2{ mtx_loginManager };
	std::lock_guard<mutex> lck3{ mtx_clients };
	std::lock_guard<mutex> lck4{ mtx_nameToSocket };

	string userName = "";
	
	_clients.erase(client_socket);

	for (auto& user : _nameToSocket)
	{
		if (user.second == client_socket)
		{
			userName = user.first;
			_nameToSocket.erase(userName);
			break;
		}
	}

	if (userName != "")
	{
		_handlerFactory.getRoomManager().removePlayer(userName);
	}


	_handlerFactory.getLoginManager().logout(userName);
	_handlerFactory.getRoomManager().CleanRooms();
}

void Communicator::sendToPlayers(vector<string> players, RequestResult result)
{
	string response(result.buffer.begin(), result.buffer.end());
	
	for (auto& name : players)
	{
		send(_nameToSocket[name], response.c_str(), response.size(), 0);
	}
}

RequestResult Communicator::execution(SOCKET clientSocket, RequestInfo requestInfo)
{
	RequestResult requestResult;

	if (!_clients[clientSocket]->isRequestRelevant(requestInfo))
	{
		requestResult.newHandler = _clients[clientSocket];
		string notRelevantMessage = "A0016{\"message\":\"ERROR\"}";
		vector<unsigned char> notRelevantBuffer(notRelevantMessage.begin(), notRelevantMessage.end());
		requestResult.buffer = notRelevantBuffer;
	}

	else if (requestInfo.id == 'B')
	{
		std::lock_guard<mutex> lck{ mtx_loginManager };
		requestResult = _clients[clientSocket]->handleRequest(requestInfo);
	}
	else if (requestInfo.id == 'C')
	{
		std::lock_guard<mutex> lck{ mtx_loginManager };
		requestResult = _clients[clientSocket]->handleRequest(requestInfo);
	}
	else if (requestInfo.id == 'D')
	{
		std::lock_guard<mutex> lck{ mtx_loginManager };
		requestResult = _clients[clientSocket]->handleRequest(requestInfo);
	}
	else if (requestInfo.id == 'E')
	{
		std::lock_guard<mutex> lck{ mtx_roomManager };
		requestResult = _clients[clientSocket]->handleRequest(requestInfo);
	}
	else if (requestInfo.id == 'F')
	{
		std::lock_guard<mutex> lck{ mtx_roomManager };
		requestResult = _clients[clientSocket]->handleRequest(requestInfo);
	}
	else if (requestInfo.id == 'G')
	{
		std::lock_guard<mutex> lck{ mtx_roomManager };
		requestResult = _clients[clientSocket]->handleRequest(requestInfo);
	
		//test.
		//std::lock_guard<mutex> lck2{ mtx_count_joinRoom };
		//number_of_join_room++;
	}
	else if (requestInfo.id == 'H')
	{
		std::lock_guard<mutex> lck{ mtx_roomManager };
		requestResult = _clients[clientSocket]->handleRequest(requestInfo);
	
		//test.
		//std::lock_guard<mutex> lck2{ mtx_count_createRoom };
		//number_of_create_room++;
	}
	else if (requestInfo.id == 'I')
	{
		std::lock_guard<mutex> lck1{ mtx_statisticsManager };
		requestResult = _clients[clientSocket]->handleRequest(requestInfo);
	
		//test.
		//std::lock_guard<mutex> lck2{ mtx_count_statistic};
		//number_of_statistic++;
	}
	else if (requestInfo.id == 'J')
	{
		std::lock_guard<mutex> lck1{ mtx_statisticsManager };
		requestResult = _clients[clientSocket]->handleRequest(requestInfo);
	}
	else if (requestInfo.id == 'K')
	{
		std::lock_guard<mutex> lck{ mtx_roomManager };
		requestResult = _clients[clientSocket]->handleRequest(requestInfo);
	}
	else if (requestInfo.id == 'L')
	{
		std::lock_guard<mutex> lck{ mtx_GameManager };
		std::lock_guard<mutex> lck1{ mtx_roomManager };
		requestResult = _clients[clientSocket]->handleRequest(requestInfo);
	}
	else if (requestInfo.id == 'M')
	{
		std::lock_guard<mutex> lck{ mtx_GameManager };
		std::lock_guard<mutex> lck1{ mtx_roomManager };
		requestResult = _clients[clientSocket]->handleRequest(requestInfo);
	}
	else if (requestInfo.id == 'N')
	{
		std::lock_guard<mutex> lck{ mtx_roomManager };
		requestResult = _clients[clientSocket]->handleRequest(requestInfo);
	}
	else if (requestInfo.id == 'Q')
	{
		std::lock_guard<mutex> lck{ mtx_GameManager };
		requestResult = _clients[clientSocket]->handleRequest(requestInfo);
	}
	else if (requestInfo.id == 'P')
	{
		std::lock_guard<mutex> lck{ mtx_GameManager };
		requestResult = _clients[clientSocket]->handleRequest(requestInfo);
	}
	else if (requestInfo.id == 'O')
	{
		std::lock_guard<mutex> lck{ mtx_GameManager };
		requestResult = _clients[clientSocket]->handleRequest(requestInfo);
	}
	else if (requestInfo.id == 'R')
	{
		std::lock_guard<mutex> lck{ mtx_GameManager };
		requestResult = _clients[clientSocket]->handleRequest(requestInfo);
	}

	return requestResult;
}

//void Communicator::display_count()
//{
//	this_thread::sleep_for(chrono::milliseconds(5000));
//	while (true)
//	{
//		std::lock_guard<mutex> lck1{ mtx_count_joinRoom };
//		std::lock_guard<mutex> lck2{ mtx_count_createRoom };
//		std::lock_guard<mutex> lck3{ mtx_count_statistic};
//
//		cout << "joinRoomCount: " << number_of_join_room << "createRoomCount: " << number_of_create_room << "statisticCount: " << number_of_statistic << "\n";
//		this_thread::sleep_for(chrono::milliseconds(3000));
//	}
//}
